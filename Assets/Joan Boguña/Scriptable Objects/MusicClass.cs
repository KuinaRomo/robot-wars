﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableMusicObject", order = 1)]
public class MusicClass : ScriptableObject
{
    public AudioClip[] audioClips;

}

