﻿
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PhotonRoom : MonoBehaviourPunCallbacks, IInRoomCallbacks, IPunObservable {

    public static PhotonRoom room;
    private PhotonView PV;

    public bool isGameLoaded;
    public int currentScene;

    Player[] photonPlayers;
    public int playersInRoom;
    public int MyNumberInRoom;

    public int PlayerInGame;

    private bool ReadyToCount;
    private bool ReadyToStart;
    public float startingTime;
    private float LessThanMaxPlayers;
    private float AtMaxPlayer;
    private float TimeToStart;
    public GameObject gameManager;
    private bool intantate;
    private bool managerIn;
    private bool dejaDeInstanciarCojones;

    private bool SendRobotB = false;

    public Robot aRobot;
    public Robot bRobot;

    public bool enviocompleto = true;

    private void Awake()
    {
        
      

        if(PhotonRoom.room == null)
        {
            PhotonRoom.room = this;
        }else
        {
            if(PhotonRoom.room != null)
            {
                Destroy(PhotonRoom.room.gameObject);
                PhotonRoom.room = this;
            }
        }
        
        DontDestroyOnLoad(this.gameObject);
    }

    

    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
        SceneManager.sceneLoaded += OnSceeneFinishLoading;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
        SceneManager.sceneLoaded -= OnSceeneFinishLoading;
    }

    // Use this for initialization
    void Start () {
        PhotonNetwork.AddCallbackTarget(this);
        PV = GetComponent<PhotonView>();
        ReadyToCount = false;
        ReadyToStart = false;
        LessThanMaxPlayers = startingTime;
        AtMaxPlayer = 6;
        TimeToStart = startingTime;

        if (SendRobotB == false)
        {
            SendRobotB = true;
        }

    }
	
	// Update is called once per frame
	void Update () {

        if (gameManager == null)
        {
            gameManager = GameObject.FindGameObjectWithTag("GameManager");
        }
        else
        {
            managerIn = true;
        }

        if (managerIn) {
            intantate = true;
            
        }   

        if (intantate && !dejaDeInstanciarCojones)
        {
            RPC_CreatePlayer();
            dejaDeInstanciarCojones = true;
        }
        if (MultiplayerSetting.multiplayerSetting.delayStart)
        {
            if(playersInRoom == 1)
            {
                RestartTimer();
            }
            if (!isGameLoaded)
            {
                if (ReadyToStart)
                {
                    AtMaxPlayer -= Time.deltaTime;
                    LessThanMaxPlayers = AtMaxPlayer;
                    TimeToStart = AtMaxPlayer;
                }
                else if (ReadyToCount)
                {
                    LessThanMaxPlayers -= Time.deltaTime;
                    TimeToStart = LessThanMaxPlayers;
                }
                Debug.Log("Display time to start to the players " + TimeToStart);
                if(TimeToStart <= 0)
                {
                    StartGame();
                }
            }
        }
	}

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("We are now in a room");
        photonPlayers = PhotonNetwork.PlayerList;
        playersInRoom = photonPlayers.Length;
        MyNumberInRoom = playersInRoom;
        PhotonNetwork.NickName = MyNumberInRoom.ToString();

        if (MultiplayerSetting.multiplayerSetting.delayStart)
        {
            Debug.Log("Displayer players in room out of max players possible (" + playersInRoom + ":" + MultiplayerSetting.multiplayerSetting.MaxPlayers + ")");
            if(playersInRoom > 1)
            {
                ReadyToCount = true;
            }
            if(playersInRoom == MultiplayerSetting.multiplayerSetting.MaxPlayers)
            {
                ReadyToStart = true;
                if (!PhotonNetwork.IsMasterClient)
                    return;
                PhotonNetwork.CurrentRoom.IsOpen = false;
            }
        }
        else
        {
            StartGame();
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log("A new Player has joined the room");
        photonPlayers = PhotonNetwork.PlayerList;
        playersInRoom++;
        if (MultiplayerSetting.multiplayerSetting.delayStart)
        {
            Debug.Log("Displayer players in room out of max players possible (" + playersInRoom + ":" + MultiplayerSetting.multiplayerSetting.MaxPlayers + ")");
            if(playersInRoom > 1)
            {
                ReadyToCount = true;
            }
            if(playersInRoom == MultiplayerSetting.multiplayerSetting.MaxPlayers)
            {
                ReadyToStart = true;
                if (!PhotonNetwork.IsMasterClient)
                    return;
                PhotonNetwork.CurrentRoom.IsOpen = false;
            }
        }
    }

    void StartGame()
    {
        isGameLoaded = true;
        if (!PhotonNetwork.IsMasterClient)
            return;
        if (MultiplayerSetting.multiplayerSetting.delayStart)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
        }
        PhotonNetwork.LoadLevel(MultiplayerSetting.multiplayerSetting.multiplayerScene);
    }

    void RestartTimer()
    {
        LessThanMaxPlayers = startingTime;
        TimeToStart = startingTime;
        AtMaxPlayer = 6;
        ReadyToCount = false;
        ReadyToStart = false;
    }

    void OnSceeneFinishLoading(Scene scene, LoadSceneMode mode)
    {
        currentScene = scene.buildIndex;
        if(currentScene == MultiplayerSetting.multiplayerSetting.multiplayerScene)
        {
            isGameLoaded = true;

            if (MultiplayerSetting.multiplayerSetting.delayStart)
            {
                PV.RPC("RPC_LoadedGameScene", RpcTarget.MasterClient);
            }else
            {
                //RPC_CreatePlayer();
            }
        }
    }

    [PunRPC]
    private void RPC_LoadedGameScene()
    {
        PlayerInGame++;
        if(PlayerInGame == PhotonNetwork.PlayerList.Length)
        {
            PV.RPC("RPC_CreatePlayer",RpcTarget.All);
        }
    }
    
    [PunRPC]
    private void RPC_CreatePlayer() {
        if (PhotonNetwork.IsMasterClient == true) {
            Robot aux = new Robot();
            GameObject robot = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Robot"), transform.position, Quaternion.identity, 0) as GameObject;
            gameManager.GetComponent<GameManager>().createAndInstantiate(gameManager.GetComponent<GameManager>().robotsAPlantilla[0], 0, 4, gameManager.GetComponent<GameManager>().robotsAPlantilla[0].getMove(), false);
            aux = gameManager.GetComponent<GameManager>().robotsA[(gameManager.GetComponent<GameManager>().robotsA.Count) - 1];
            gameManager.GetComponent<GameManager>().initRobot(robot, 0, 4, gameManager.GetComponent<GameManager>().robotsAPlantilla[0].getMove(), aux);
            //aux.setRobot(robot);
            //robot.transform.position = gameManager.GetComponent<GameManager>().casillas[aux.getCol()][aux.getRow()].transform.position;
            robot.transform.position = new Vector3(aux.getCol(), aux.getRow(), robot.transform.position.z);
            int temp = GameObject.FindGameObjectsWithTag("robotA").Length + GameObject.FindGameObjectsWithTag("robotB").Length;
            robot.GetComponent<dondeMover>().myId = temp;
            aux.setId(temp);
            gameManager.GetComponent<GameManager>().robotsControled = "robotA";
            //aux.getDataSend();
            /*robot.GetComponent<dondeMover>().numCol = aux.getCol();
            robot.GetComponent<dondeMover>().numFila = aux.getRow();*/
        } 

        else {
            Robot aux2 = new Robot();
            GameObject robot2 = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "RobotB"), transform.position, Quaternion.identity, 0) as GameObject;
            gameManager.GetComponent<GameManager>().createAndInstantiate(gameManager.GetComponent<GameManager>().robotsBPlantilla[0], 1, 4, gameManager.GetComponent<GameManager>().robotsBPlantilla[0].getMove(), true);
            aux2 = gameManager.GetComponent<GameManager>().robotsB[(gameManager.GetComponent<GameManager>().robotsB.Count) - 1];
            gameManager.GetComponent<GameManager>().initRobot(robot2, 1, 4, gameManager.GetComponent<GameManager>().robotsBPlantilla[0].getMove(), aux2);
            //aux2.setRobot(robot2);
            //robot2.transform.position = gameManager.GetComponent<GameManager>().casillas[aux2.getCol()][aux2.getRow()].transform.position;
            robot2.transform.position = new Vector3(aux2.getCol(), aux2.getRow(), robot2.transform.position.z);
            gameManager.GetComponent<GameManager>().sendInformation(aux2.getDataSend());
            gameManager.GetComponent<GameManager>().robotsControled = "robotB";
            int temp = GameObject.FindGameObjectsWithTag("robotA").Length + GameObject.FindGameObjectsWithTag("robotB").Length;
            robot2.GetComponent<dondeMover>().myId = temp;
            aux2.setId(temp);
            /*robot2.GetComponent<dondeMover>().numCol = aux2.getCol();
            robot2.GetComponent<dondeMover>().numFila = aux2.getRow();*/
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        
    }
}
