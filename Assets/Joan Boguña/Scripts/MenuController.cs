﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MenuController : MonoBehaviour {

    [SerializeField] private Canvas menuCanvas;
    [SerializeField] private Canvas optionsCanvas;
    [SerializeField] private Canvas CreditsCanvas;
    [SerializeField] private GameManager gameManager;
    GameObject PauseMenu;
    private int[] ScreenResolutionW;
    private int[] ScreensResolutionH;
    [SerializeField] private UnityEngine.UI.Dropdown dropdown;
    public GameObject music;

    public void Start()
    {
        ScreenResolutionW = new int[8];
        ScreenResolutionW[0] = 1366;
        ScreenResolutionW[1] = 1920;
        ScreenResolutionW[2] = 1024;
        ScreenResolutionW[3] = 1280;
        ScreenResolutionW[4] = 1440;
        ScreenResolutionW[5] = 1280;
        ScreenResolutionW[6] = 1600;
        ScreenResolutionW[7] = 1680;

        ScreensResolutionH = new int[8];
        ScreensResolutionH[0] = 768;
        ScreensResolutionH[1] = 1080;
        ScreensResolutionH[2] = 768;
        ScreensResolutionH[3] = 1024;
        ScreensResolutionH[4] = 900;
        ScreensResolutionH[5] = 800;
        ScreensResolutionH[6] = 900;
        ScreensResolutionH[7] = 1050;
    }
    
    public void Update()
    {
      
        switch (dropdown.value)
        {
            case 0:
                changeScreen(ScreenResolutionW[0], ScreensResolutionH[0]);
                break;
            case 1:
                changeScreen(ScreenResolutionW[1], ScreensResolutionH[1]);
                break;
            case 2:
                changeScreen(ScreenResolutionW[2], ScreensResolutionH[2]);
                break;
            case 3:
                changeScreen(ScreenResolutionW[3], ScreensResolutionH[3]);
                break;
            case 4:
                changeScreen(ScreenResolutionW[4], ScreensResolutionH[4]);
                break;
            case 5:
                changeScreen(ScreenResolutionW[5], ScreensResolutionH[5]);
                break;
            case 6:
                changeScreen(ScreenResolutionW[6], ScreensResolutionH[6]);
                break;
            case 7:
                changeScreen(ScreenResolutionW[7], ScreensResolutionH[7]);
                break;
            default:
                break;
        }
    }

    
    public void NewGameDisableButton() {
        music.GetComponent<MusicControler>().audioSource[1].clip = music.GetComponent<MusicControler>().Music.audioClips[1];
        music.GetComponent<MusicControler>().audioSource[1].Play();
        SceneManager.LoadScene("CreationMenu");

    }
    
    public void optionsDisableButton() {
        music.GetComponent<MusicControler>().audioSource[1].clip = music.GetComponent<MusicControler>().Music.audioClips[1];
        music.GetComponent<MusicControler>().audioSource[1].Play();
        menuCanvas.gameObject.SetActive(false);
        optionsCanvas.gameObject.SetActive(true);
    }

    public void CreditsDisableButton() {
        music.GetComponent<MusicControler>().audioSource[1].clip = music.GetComponent<MusicControler>().Music.audioClips[1];
        music.GetComponent<MusicControler>().audioSource[1].Play();
        menuCanvas.gameObject.SetActive(false);
        CreditsCanvas.gameObject.SetActive(true);
    }

    public void ExitGameButton() {
        music.GetComponent<MusicControler>().audioSource[1].clip = music.GetComponent<MusicControler>().Music.audioClips[1];
        music.GetComponent<MusicControler>().audioSource[1].Play();
        Application.Quit();
    }

    public void changeScreen(int ScreenX,int ScreenY) {
        Screen.SetResolution(ScreenX, ScreenY,true);
    }
}
