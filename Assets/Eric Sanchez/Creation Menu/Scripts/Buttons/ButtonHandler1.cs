﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ButtonHandler1 : MonoBehaviour {
    Text RobotSelect;
    Button btn;
    public GameObject[] robotConfig;
    MusicControler musicManager;

	// Use this for initialization
	void Start () {
        RobotSelect = GameObject.Find("TxtCurrentRobotEdit").GetComponent<Text>();
        btn = this.gameObject.GetComponent<Button>();
        btn.onClick.AddListener(delegate { onClickButton(btn); });

        musicManager = GameObject.Find("MusicController").GetComponent<MusicControler>();
	}
	
	// Update is called once per frame
	void onClickButton(Button btnClicked)
    {
        for(int i = 0; i < robotConfig.Length; i++)
        {
            robotConfig[i].SetActive(false);
        }
        switch (btnClicked.name)
        {
            case "BtnRobot1":
               
                robotConfig[0].SetActive(true);
                break;
            case "BtnRobot2":
              
                robotConfig[1].SetActive(true);
                break;
            case "BtnRobot3":
               
                robotConfig[2].SetActive(true);
                break;
            case "BtnRobot4":
                
                robotConfig[3].SetActive(true);
                break;
            case "BtnRobot5":
               
                robotConfig[4].SetActive(true);
                break;
        }


        musicManager.audioSource[1].clip = musicManager.Music.audioClips[1];
        musicManager.audioSource[1].Play();
        
    }
}
