﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class ComponentController : MonoBehaviour {
    [Header("Buttons")]
    [SerializeField] Button prevBtn;
    [SerializeField] Button nextBtn;

    [Header("Image components")]
    [SerializeField] Image prevComp;
    [SerializeField] Image selComp;
    [SerializeField] Image nextComp;

    [Header("Configuration parent")]
    [SerializeField] GameObject configuration;

    MusicControler musicManager;


    AsembleRobot asR;

    public enum SPECIALS { NULL, volador, noDiagonales }; 
    public enum Type { NULL, Tank, Aerodynamic, Normal, UFO};
    
    [System.Serializable]
    public class Component
    {
        public Type type;
        public Sprite[] img;
        public int armor;
        public int attack;
        public int weight;
        public int resourcesCost;
        public SPECIALS enumerator;
        public Type notCompatibleWith;
        public TypeRobot[] typeRobot;
        public RobotZone robotZone;
        public RobotWeapons robotWeapon;
    }
    [Header("Array of Components")]
    public Component[] components;

    [Header("Selected Component")]
    public Component selected;

    int componentsIndex;



	// Use this for initialization
	void Start ()
    {
        prevBtn = transform.Find("PrevBtn").gameObject.GetComponent<Button>();
        nextBtn = transform.Find("NextBtn").gameObject.GetComponent<Button>();

        prevComp = transform.Find("Prev").gameObject.GetComponent<Image>();
        selComp  = transform.Find("Selected").gameObject.GetComponent<Image>();
        nextComp = transform.Find("Next").gameObject.GetComponent<Image>();

        configuration = transform.parent.gameObject;

        prevBtn.onClick.AddListener(delegate { SetPrevComp(); });
        nextBtn.onClick.AddListener(delegate { SetNextComp(); });
        asR = configuration.GetComponent<AsembleRobot>();
        componentsIndex = 0;
        selected = components[0];

        musicManager = GameObject.Find("MusicController").GetComponent<MusicControler>();

    }

    void SetPrevComp()
    {
        componentsIndex -= 1;
        if (componentsIndex < 0) componentsIndex = components.Length-1;
        UpdateComponents();
    }

    void SetNextComp()
    {
        componentsIndex += 1;
        if (componentsIndex > components.Length-1) componentsIndex = 0;
        UpdateComponents();
    }

    void UpdateComponents()
    {

        musicManager.audioSource[1].clip = musicManager.Music.audioClips[1];
        musicManager.audioSource[1].Play();

        if (componentsIndex == 0)
        {
            prevComp.sprite = components[components.Length - 1].img[0];
        }
        else prevComp.sprite = components[componentsIndex - 1].img[0];

        selComp.sprite = components[componentsIndex].img[0];

        if (componentsIndex == components.Length - 1)
        {
            nextComp.sprite = components[0].img[0];
        }
        else nextComp.sprite = components[componentsIndex + 1].img[0];

        selected = components[componentsIndex];
        asR.BuildRobot();
    }
	
}
