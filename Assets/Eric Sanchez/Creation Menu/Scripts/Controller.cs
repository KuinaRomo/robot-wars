﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller  {
    public enum SPECIALS { NULL, volador };

    public Sprite img;
    public int armor;
    public int attack;
    public int weight;
    public int resourcesCost;
    public SPECIALS enumerator;
}
