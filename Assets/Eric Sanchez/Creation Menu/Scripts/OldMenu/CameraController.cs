﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    // Use this for initialization
    // Attach this script to an orthographic camera.

    Transform obj;     // The object we will move.
    Vector3 offSet;       // The object's position relative to the mouse position.
    float dist;
    [SerializeField] private Camera cam;
    [SerializeField] private Vector3 rescale;
    ObjMenuHandler scrObj;
    Vector2 objSize;
    Vector2 objPos, objInitPos;
    GameObject[] holders;
    Vector2 sizeHolders;
    Vector2 positionHolders;



    void Start()
    {
          
    }
    void Update()
    {

        var ray = cam.ScreenPointToRay(Input.mousePosition);     // Gets the mouse position in the form of a ray.

        if (Input.GetMouseButtonDown(0))
        {     // If we click the mouse...
            Debug.Log("MouseDown");
            
                Ray R = cam.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(R, out hit) && (hit.collider.tag == "Draggable"))
                {        // Then see if an object is beneath us using raycasting.
                    Debug.Log("Raycast");
                
                   
                obj = hit.transform;        // If we hit an object then hold on to the object.
                if (!scrObj)scrObj = obj.GetComponent<ObjMenuHandler>();     //Checks if the script is already selected
                objInitPos = obj.position;
                obj.localScale = rescale;   //Rescale the obj to make it bigger
                offSet = obj.position - hit.point;        // This is so when you click on an object its center does not align with mouse position.
                Debug.Log("Moving");
                dist = (ray.origin - hit.point).magnitude;  // Distance to the object from ray origin.
                }
            
            }

        

        else if (Input.GetMouseButtonUp(0))
        {
           
            Debug.Log("MouseUp");
            if (obj != null && obj.tag == "Draggable")
            {
                obj.localScale = new Vector3(1, 1, 1);    //Rescale the obj to return at his normal size
                objSize = obj.GetComponent<Renderer>().bounds.size;
                objPos = obj.position; 
                holders = obj.GetComponent<ObjMenuHandler>().holders;
                for(int i = 0; i < holders.Length; i++)
                {
                    sizeHolders = holders[i].GetComponent<Renderer>().bounds.size;
                    positionHolders = holders[i].GetComponent<Transform>().position;
                    Debug.Log("Size Holders "+i+" : "+sizeHolders);
                    if(objPos.x >= positionHolders.x - sizeHolders.x/2 && objPos.x <= positionHolders.x + sizeHolders.x/2 
                        && objPos.y <= positionHolders.y + sizeHolders.y && objPos.y >= positionHolders.y - sizeHolders.y)
                    {
                        Debug.Log("Align the obj");
                        obj.position = objPos = positionHolders;
                        holders[i].GetComponent<HolderController>().setItem(obj);
                    }
                    else
                    {
                        obj.GetComponent<ObjMenuHandler>().resetPos();
                    }
                }
            }
            obj = null;     // Let go of the object.
            scrObj = null;  // Release the script

        }
        
        if (obj)
        {
            obj.position = ray.GetPoint(dist) + offSet;    // Only move the object on a 2D plane.
        }

    }
}
