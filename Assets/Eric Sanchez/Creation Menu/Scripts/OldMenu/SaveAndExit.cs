﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SaveAndExit : MonoBehaviour {
    List<GameObject> pieceT = new List<GameObject>();
    List<List<GameObject>> configs = new List<List<GameObject>>();
    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(delegate { exportRobots(); });
    }

   void exportRobots()
    {
        
        for (int j = 1; j < 5; j++)
        {
            for (int i = 0; i < GameObject.FindGameObjectsWithTag("Holder").Length; i++)
            {  
                GameObject holder = GameObject.FindGameObjectWithTag("Holder");
                Debug.Log( holder.name);
                Transform holderT = holder.GetComponent<Transform>();
                GameObject parentH = holderT.parent.gameObject;
                HolderController hc = holder.GetComponent<HolderController>();
                if (!hc.currObject) Debug.Log("Tienes que colocar todos los modulos");
                if(parentH.name == "RobotConf" + j)
                {

                    pieceT.Add(holder);
                    Debug.Log("Parent name" +parentH.name);
                }
            }
            
            configs.Add(pieceT);
        }
        
    }
}
