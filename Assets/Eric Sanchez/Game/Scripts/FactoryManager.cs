﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactoryManager : MonoBehaviour {

    //This script will handle the factory behaviour
    public float actualResources, topResources;
    public float life, maxLife , resourcesProductivity; //Resources productivity is the amount of resources that will be produced every time the player gets the turn
    public bool isFriendly; //This variable will indicate if the factory is on the controll of allies, if is on controll the resources will be made else not
    public bool isB;
    public ResourcesManager RM; //We call the resources manager to modify the actual resources on the bar and the value itself
    [SerializeField] GameObject GameManager;
    public List<GameObject> positions;
    public string robotType;
    public bool death;
    
	// Use this for initialization
	void Start () {
        death = false;
        life = maxLife;
        isFriendly = true; //We are setting this variable to true just to avoid any compilation error, what will set and manage this variable will be the game manager
        RM.renewResources(topResources, actualResources);
    }
	
	// Update is called once per frame
	void Update () {
		if(life <= 0) {
            if (isFriendly) isFriendly = false;
            else isFriendly = true;
            
            life = maxLife;
            
        }
	}

    //This function needs to be called from another script in order to increase the resources

    public void createResources(bool aumentando) {
        //Debug.Log(RM.actualResources + resourcesProductivity);
        if (isFriendly) {
            if (aumentando) {
                if ((actualResources + resourcesProductivity) < topResources) {
                    actualResources += resourcesProductivity; //If the factory is friendly it will increase the actual resources 
                }
                else {
                    actualResources = topResources;
                }
                if(GameManager.GetComponent<GameManager>().robotTurn == "robotB") {
                    GameManager.GetComponent<GameManager>().resourcesB = actualResources;
                }
            }
           
            RM.renewResources(topResources, actualResources);
        }
    }
}
