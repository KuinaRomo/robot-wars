﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniRobotsController : MonoBehaviour {

    [SerializeField] GameObject PrefabMiniRobot;
    [SerializeField] GameObject PrefabMiniEnemy;
    GameObject robotToMove;
    public bool calculated;
    

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        
    }

    public GameObject instantiateMiniRobot(bool enemy, float posX, float posY) {
        GameObject miniRobot;
        if (enemy) {
            miniRobot = Instantiate(PrefabMiniEnemy);
        }
        else {
            miniRobot = Instantiate(PrefabMiniRobot);
        }
        miniRobot.transform.SetParent(this.transform);
        miniRobot.GetComponent<RectTransform>().localPosition = new Vector3(posX * 4, posY * 4, 0);
        miniRobot.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        return miniRobot;
    }

    public void moveMiniRobot(float finalX, float finalY) {
        robotToMove.GetComponent<RectTransform>().localPosition = new Vector3(finalX * 4, finalY * 4, 0);
    }

    public void whatRobot(GameObject robotMoved) {
        foreach(Transform child in transform) {
            Debug.Log("La ID del robot movido es: " + robotMoved.GetComponent<dondeMover>().miniRobot);
            if (robotMoved.GetComponent<dondeMover>().miniRobot == child.GetComponent<miniRobotsID>().myID) {
                Debug.Log("El game object que muevo es: " + child.gameObject.name);
                robotToMove = child.gameObject;
            }
        }
        
    }
}
