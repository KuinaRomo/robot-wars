﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class addChild : MonoBehaviour {

    [SerializeField] GameObject lifePrefab;

	// Use this for initialization
	void Start () {
        this.transform.parent = lifePrefab.transform;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
