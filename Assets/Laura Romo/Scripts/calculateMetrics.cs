﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class calculateMetrics : MonoBehaviour {

    public int spawnedUnitiesA;
    public int spawnedUnitiesB;
    public int numAttacksA;
    public int numAttacksB;
    public int totalPowerAttackA;
    public int totalPowerAttackB;
    public int totalLostLiveA;
    public int totalLostLiveB;
    public int robotsLostA;
    public int robotsDestroyedA;
    public int robotsLostB;
    public int robotsDestroyedB;
    public int totalTurnsA;
    public int totalTurnsB;
    public float totalTime;
    public bool empezarRecuento = false;

    [SerializeField] List<string> UItext;
    private int actualPosition = 0;
    private int actualNumA = 0;
    private int actualNumB = 0;
    private int totalDecimals = 0;
    private int totalCeros = 0;
    private int showNumA = 0;
    private int showNumB = 0;
    GameObject player;
    GameObject CPU;
    MusicControler musicController;
    private string actualString;
    private bool subidoA = false;
    private bool subidoB = false;
    private bool showDeaths = false;
    private bool cambiado = false;

    // Use this for initialization
    void Start () {
        spawnedUnitiesA = 0;
        spawnedUnitiesB = 0;
        numAttacksA = 0;
        numAttacksB = 0;
        totalPowerAttackA = 0;
        totalPowerAttackB = 0;
        totalLostLiveA = 0;
        totalLostLiveB = 0;
        robotsLostA = 0;
        robotsDestroyedA = 0;
        robotsLostB = 0;
        robotsDestroyedB = 0;
        totalTurnsA = 0;
        totalTurnsB = 0;
        totalTime = 0;
        DontDestroyOnLoad(gameObject);
        musicController = GameObject.Find("MusicController").GetComponent<MusicControler>();
    }

    // Update is called once per frame
    void Update() {
        if (SceneManager.GetActiveScene().name == "MetricsScene" && !cambiado) {
            cambiado = true;
            empezarRecuento = true;
            player = GameObject.Find("Player");
            CPU = GameObject.Find("CPU");
        }

        //Subir el primero
        if (empezarRecuento) {
            Debug.Log("Sigo entrando");
            if(!subidoA || !subidoB) {
                musicController.audioSource[1].Stop();
                musicController.audioSource[1].loop = true;
                musicController.audioSource[1].clip = musicController.Music.audioClips[9];
                musicController.audioSource[1].Play();
                //InvokeRepeating("subirNumsA", 0f, 5f);
                InvokeRepeating("subirNumsB", 0f, 5f);
                if (subidoA) {
                    CancelInvoke("subirNumsA");
                }
                if (subidoB) {
                    CancelInvoke("subirNumsB");
                }
            }
            else {
                actualPosition++;
                Debug.Log("De actual position is: " + actualPosition);
                if(actualPosition < 6) {
                    cambiarNumA();
                    cambiarNumB();
                    subidoA = false;
                    subidoB = false;
                }
                else {
                    musicController.audioSource[1].Stop();
                    CancelInvoke("subirNumsA");
                    CancelInvoke("subirNumsB");
                    empezarRecuento = false;
                    subidoA = false;
                    subidoB = false;
                }
            }
        }
	}

    void cambiarNumA() {
        switch (actualPosition) {
            case 0:
                showNumA = spawnedUnitiesA;
                break;

            case 1:
                showNumA = numAttacksA;
                break;

            case 2:
                showNumA = totalPowerAttackA;
                break;

            case 3:
                showNumA = totalLostLiveA;
                break;

            case 4:
                //showDeaths = true;
                showNumA = robotsDestroyedA;
                break;

            case 5:
                showNumA = totalTurnsA;
                break;

            default:
                Debug.Log("Si llegas aqui te has pasado");
                break;
        }
    }

    void cambiarNumB() {
        switch (actualPosition) {
            case 0:
                showNumB = spawnedUnitiesB;
                break;

            case 1:
                showNumB = numAttacksB;
                break;

            case 2:
                showNumB = totalPowerAttackB;
                break;

            case 3:
                showNumB = totalLostLiveB;
                break;

            case 4:
                showNumB = robotsDestroyedB;
                break;

            case 5:
                showNumB = totalTurnsB;
                break;

            default:
                Debug.Log("Si llegas aqui te has pasado");
                break;
        }
    }

    int totalNums(int num) {
        int totalDecimals = 0;
        int result = -1;
        while(result != 0) {
            result = num / 10;
            totalDecimals++;
            num = result;
        }
        return totalDecimals;
    }

    void subirNumsA() {
        //CAMBIAR ESTO
        if(actualPosition < 6){
            actualString = "";
            if(actualNumA < showNumA) {
                actualNumA++;
                totalDecimals = totalNums(actualNumA);
                if(totalDecimals < 0) {
                    totalDecimals = 0;
                }
                totalCeros = 5 - totalDecimals;
                if(totalCeros < 0) {
                    totalCeros = 0;
                }
                Debug.Log(actualPosition);
                actualString = UItext[actualPosition];
                for(int i = 0; i < totalCeros; i++) {
                    actualString += '0';
                }
                actualString += actualNumA;
                player.transform.GetChild(actualPosition).GetComponent<TextMeshProUGUI>().text = actualString;
            }

            else {
                actualNumA = 0;
                subidoA = true;
            }
        }
    }

    void subirNumsB() {
        //CAMBIAR ESTO
        if (actualPosition < 6) {
            actualString = "";
            if (actualNumB < showNumB) {
                actualNumB++;
                totalDecimals = totalNums(actualNumB);
                if (totalDecimals < 0) {
                    totalDecimals = 0;
                }
                totalCeros = 5 - totalDecimals;
                if (totalCeros < 0) {
                    totalCeros = 0;
                }
                actualString = UItext[actualPosition];
                for (int i = 0; i < totalCeros; i++) {
                    actualString += '0';
                }
                actualString += actualNumB;
                CPU.transform.GetChild(actualPosition).GetComponent<TextMeshProUGUI>().text = actualString;
            }

            else {
                actualNumB = 0;
                subidoA = true;
                subidoB = true;
            }
        }
    }
}
