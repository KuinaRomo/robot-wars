﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using System;

public class GameManager : MonoBehaviour, IOnEventCallback, IPunObservable {
    public string robotTurn;  /*!< The actual turn */ 
    public bool selected;   /*!< Check if there are any robot selected */
    private bool inPlaceX;  /*!< Check if the robot arriba to his final X */
    private bool inPlaceY;  /*!< Check if the robot arriba to his final Y */
    private bool calculated;   /*!< Check if the final position is calcualted */
    private bool sended;    /*!< Check if the online information is sended */
    public GameObject robotSelect;  /*!< The robot that the player has select */
    [SerializeField] private GameObject tablero; /*!< Reference to the GameObject tablero */
    [SerializeField] private GameObject actionHUD; /*!< Reference to the GameObject actionHUD */
    public List<List<GameObject>> casillas; /*!< List of all the individeual boxes of the gameboard in the game */
    private Transform myTransform;  /*!< Element transform of the gameboard */
    public bool moving; /*!< Check if one robot is moving */
    public GameObject casillaObjetivo; /*!< The box where the robot goes to */
    private float contadorMoving; /*!< How many times the robot is moved to t objective */
    private float moveX; /*!< How the robot have to move in the x coordinate */
    private float moveY; /*!< How the robot have to move in the y coordinate */
    private int times;
    public int totalMovido;
    private int manhattanDistance;
    private int attackRestriction;
    private int boxSize;
    public int nextMinirobotID;
    public bool online;
    public bool tirarIA;
    public bool atacando;
    public List<Robot> robotsA;
    public List<Robot> robotsB;
    public List<Robot> robotsAPlantilla;
    public List<Robot> robotsBPlantilla;
    public List<GameObject> miniRobots;
    List<Robot> temporalList;
    private List<GameObject> factories;
    public GameObject[] miniFactoriesB;
    private float actualLife;
    //variable temporal
    private Robot robot;
    public GameObject selectedFactory;
    [SerializeField] GameObject canvasSelectRobot;
    [SerializeField] MiniRobotsController miniRobotsController;
    [SerializeField] IAController IAController;
    [SerializeField] aStar aStarScript;
    [SerializeField] calculateMetrics Metrics;
    [SerializeField] CameraMovement cameraMovement;
    [SerializeField] RobotParts robotParts;
    [SerializeField] GameObject casillaAttack;
    [SerializeField] GameObject casillaMove;
    public GameObject pauseMenu;

    public PhotonRoom photonRoom;
    private GameObject selectedBox;
    [SerializeField] ResourcesManager RM;
    public float resourcesB;
    public bool destruido = false;
    public bool calculado = false;
    float initialTime;
    float finalTime;
    private int position = 0;
    private int contadorPlantillas = 0;

    //Magic strings
    private string nameScene = "Multiplayer";
    private string tagRobotA = "robotA";
    private string tagRobotB = "robotB";
    private string moveTag = "move";
    private string ocupadaTag = "ocupada";
    private string blockTag = "block";
    private string noTag = "Untagged";

    //Magic numbers
    private float maxCount = 10000f;
    private int stepsToObjective = 10;
           

    //List of all attacks (Rellenar al principio?)
    private List<Attacks> allAttacks = new List<Attacks>();
    private GameObject auxGameObj;
    private GameObject miniRobotActual;
    private GameObject[] miniFactoriesA;
    private GameObject robotExplote;
    public MusicControler musicController;

    //RobotID
    public int robotID;

    //Variable para online
    public string robotsControled;

    public List<GameObject> FactoriesA;
    public List<GameObject> FactoriesB;
    public List<GameObject> pathWay;
    GameObject[] water;
    List<string> badTags = new List<string>();
    public bool test;
        
    void OnEnable() {
        PhotonNetwork.AddCallbackTarget(this);
    }
    void OnDisable() {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    // Use this for initialization
    void Start() {
        musicController = GameObject.Find("MusicController").GetComponent<MusicControler>();
        test = false;
        initialTime = Time.time;
        //resourcesB = FactoriesB[0].GetComponent<FactoryManager>().topResources;

        miniFactoriesB = GameObject.FindGameObjectsWithTag("miniFactoryB");
        miniFactoriesA = GameObject.FindGameObjectsWithTag("miniFactoryA");

        factories = new List<GameObject>();
        badTags = new List<string>();
        badTags.Add("factory");
        badTags.Add("ocupada");
        badTags.Add("block");
        robotID = 0;
        nextMinirobotID = 0;
        if (online) { 
            photonRoom = GameObject.Find("RoomController").GetComponent<PhotonRoom>();
        }
        PhotonNetwork.AddCallbackTarget(this);
        sended = false;
        //Poner por codigo después
        if (SceneManager.GetActiveScene().name == nameScene) {
            online = true;
        }
        else {
            online = false;
        }

        casillas = new List<List<GameObject>>();
        water = GameObject.FindGameObjectsWithTag("water");
        robotTurn = tagRobotA;
        selected = false;
        moving = false;
        contadorMoving = 0;
        times = 0;
        boxSize = 1;
        myTransform = tablero.GetComponent<Transform>();
        aStarScript = GetComponent<aStar>();
        /*robotsA = new List<GameObject>();
        robotsA.AddRange(GameObject.FindGameObjectsWithTag("robotA"));
        robotsB = new List<GameObject>();
        robotsB.AddRange(GameObject.FindGameObjectsWithTag("robotB"));*/

        //Creo robots y los grado en las list. Parte temporal. Luego se crearan y meteran desde el
        //menu donde los creas.
        temporalList = new List<Robot>();
        robotsA = new List<Robot>();
        robotsAPlantilla = new List<Robot>();

        SaveAndPlay sap;
        sap = GameObject.Find("Save&Play").GetComponent<SaveAndPlay>();

        for (int i = 0; i < sap.asR.Length; i++) {
            ComponentController.Component comp = sap.asR[i].finalComponent;
            createPlantilla(comp.armor, comp.weight, 0, comp.resourcesCost, comp.typeRobot[0], comp.typeRobot[1], comp.typeRobot[2], comp.robotWeapon, 100, 1, true, true, i);
        }



        //createPlantilla(200, 4, 1, 40, TypeRobot.AERODYNAMIC, TypeRobot.NORMAL, TypeRobot.OVNI, RobotWeapons.GUN, 100, 2, true, true, 0);
        //createPlantilla(200, 4, 1, 40, TypeRobot.OVNI, TypeRobot.TANK, TypeRobot.TANK, RobotWeapons.KNIFE, 110, 2, true, true, 1);
        //createPlantilla(200, 4, 1, 40, TypeRobot.TANK, TypeRobot.OVNI, TypeRobot.OVNI, RobotWeapons.GUN, 100, 2, true, true, 2);
        //createPlantilla(200, 4, 1, 40, TypeRobot.OVNI, TypeRobot.TANK, TypeRobot.TANK, RobotWeapons.KNIFE, 110, 2, true, true, 3);

        //PLANTILLAS PARA ENEMIGOS IA 
        createPlantilla(100, 3, 1, 60, TypeRobot.NORMAL, TypeRobot.TANK, TypeRobot.AERODYNAMIC, RobotWeapons.SWORD, 60, 2, true, false, 4);
        createPlantilla(120, 2, 1, 50, TypeRobot.TANK, TypeRobot.NORMAL, TypeRobot.OVNI, RobotWeapons.KNIFE, 80, 1, true, false, 4);
        createPlantilla(100, 3, 1, 60, TypeRobot.NORMAL, TypeRobot.TANK, TypeRobot.AERODYNAMIC, RobotWeapons.SWORD, 60, 2, true, false, 4);
        createPlantilla(120, 2, 1, 50, TypeRobot.TANK, TypeRobot.NORMAL, TypeRobot.OVNI, RobotWeapons.KNIFE, 80, 1, true, false, 4);
        createPlantilla(90, 3, 1, 50, TypeRobot.AERODYNAMIC, TypeRobot.OVNI, TypeRobot.NORMAL, RobotWeapons.MACHINEGUN, 50, 2, true, false, 4);
        createPlantilla(80, 4, 1, 50, TypeRobot.OVNI, TypeRobot.AERODYNAMIC, TypeRobot.TANK, RobotWeapons.GUN, 70, 2, true, false, 4);
        createPlantilla(120, 3, 1, 50, TypeRobot.TANK, TypeRobot.NORMAL, TypeRobot.OVNI, RobotWeapons.SHOTGUN, 40, 3, true, false, 4);
        createPlantilla(80, 2, 1, 50, TypeRobot.OVNI, TypeRobot.TANK, TypeRobot.NORMAL, RobotWeapons.SWORD, 60, 2, true, false, 4);
        createPlantilla(90, 4, 1, 50, TypeRobot.AERODYNAMIC, TypeRobot.AERODYNAMIC, TypeRobot.AERODYNAMIC, RobotWeapons.KNIFE, 80, 1, true, false, 4);
        createPlantilla(80, 3, 1, 50, TypeRobot.OVNI, TypeRobot.OVNI, TypeRobot.OVNI, RobotWeapons.MACHINEGUN, 50, 2, true, false, 4);

        totalMovido = 0;

        //Debug.Log("Cada vez que se crea un robot meter dentro de las list robotsA un equipo y robotsB el otro.");
        //Haces una lista de una lista que contenga las casillas separadas por filas
        for (int i = 0; i < myTransform.childCount; i++) {
            casillas.Add(new List<GameObject>());
            for (int j = 0; j < myTransform.GetChild(i).childCount; j++) {
                casillas[i].Add(myTransform.GetChild(i).GetChild(j).gameObject);
            }
        }

        putMoveAndAttack();

        if (!online) {
            /*createAndInstantiate(robotsBPlantilla[0], 0, 8, robotsBPlantilla[0].getMove(), true);
            miniRobotActual = miniRobotsController.instantiateMiniRobot(true, 8, 0);
            miniRobots.Add(miniRobotActual);
            casillas[0][8].tag = "ocupada";

            createAndInstantiate(robotsBPlantilla[1], 1, 4, robotsBPlantilla[1].getMove(), true);
            miniRobotActual = miniRobotsController.instantiateMiniRobot(true, 4, 1);
            miniRobots.Add(miniRobotActual);
            casillas[1][4].tag = "ocupada";*/

            //Creando robots del level 1
            createAndInstantiate(robotsBPlantilla[0], 2, 4, robotsBPlantilla[0].getMove(), true);
            miniRobotActual = miniRobotsController.instantiateMiniRobot(true, 4, 2);
            miniRobots.Add(miniRobotActual);
            casillas[2][4].tag = "ocupada";

            createAndInstantiate(robotsBPlantilla[1], 4, 7, robotsBPlantilla[1].getMove(), true);
            miniRobotActual = miniRobotsController.instantiateMiniRobot(true, 7, 4);
            miniRobots.Add(miniRobotActual);
            casillas[4][7].tag = "ocupada";

            createAndInstantiate(robotsBPlantilla[2], 2, 15, robotsBPlantilla[2].getMove(), true);
            miniRobotActual = miniRobotsController.instantiateMiniRobot(true, 15, 2);
            miniRobots.Add(miniRobotActual);
            casillas[2][15].tag = "ocupada";

            createAndInstantiate(robotsBPlantilla[3], 5, 16, robotsBPlantilla[3].getMove(), true);
            miniRobotActual = miniRobotsController.instantiateMiniRobot(true, 16, 5);
            miniRobots.Add(miniRobotActual);
            casillas[5][16].tag = "ocupada";

            createAndInstantiate(robotsBPlantilla[4], 0, 20, robotsBPlantilla[4].getMove(), true);
            robotsB[robotsB.Count - 1].getRobot().GetComponent<enemyPosition>().objetivoFabrica = true;
            robotsB[robotsB.Count - 1].getRobot().GetComponent<enemyPosition>().objetivoRobots = false;
            miniRobotActual = miniRobotsController.instantiateMiniRobot(true, 20, 0);
            miniRobots.Add(miniRobotActual);
            casillas[0][20].tag = "ocupada";

            createAndInstantiate(robotsBPlantilla[5], 4, 29, robotsBPlantilla[5].getMove(), true);
            robotsB[robotsB.Count - 1].getRobot().GetComponent<enemyPosition>().objetivoFabrica = true;
            robotsB[robotsB.Count - 1].getRobot().GetComponent<enemyPosition>().objetivoRobots = false;
            miniRobotActual = miniRobotsController.instantiateMiniRobot(true, 29, 4);
            miniRobots.Add(miniRobotActual);
            casillas[4][29].tag = "ocupada";

            createAndInstantiate(robotsBPlantilla[6], 1, 30, robotsBPlantilla[6].getMove(), true);
            robotsB[robotsB.Count - 1].getRobot().GetComponent<enemyPosition>().objetivoFabrica = true;
            robotsB[robotsB.Count - 1].getRobot().GetComponent<enemyPosition>().objetivoRobots = false;
            miniRobotActual = miniRobotsController.instantiateMiniRobot(true, 30, 1);
            miniRobots.Add(miniRobotActual);
            casillas[1][30].tag = "ocupada";

            createAndInstantiate(robotsBPlantilla[7], 2, 34, robotsBPlantilla[7].getMove(), true);
            robotsB[robotsB.Count - 1].getRobot().GetComponent<enemyPosition>().objetivoFabrica = true;
            robotsB[robotsB.Count - 1].getRobot().GetComponent<enemyPosition>().objetivoRobots = false;
            miniRobotActual = miniRobotsController.instantiateMiniRobot(true, 34, 2);
            miniRobots.Add(miniRobotActual);
            casillas[2][34].tag = "ocupada";
            //createAndInstantiate(robotsBPlantilla[1], 4, 4, robotsBPlantilla[1].getMove(), true);
        }

        //Catch factories
        for(int i = 0; i < GameObject.FindGameObjectsWithTag("factoryA").Length; i++) {
            factories.Add(GameObject.FindGameObjectsWithTag("factoryA")[i]);
        }

        for (int i = 0; i < GameObject.FindGameObjectsWithTag("factoryB").Length; i++) {
            factories.Add(GameObject.FindGameObjectsWithTag("factoryB")[i]);
        }
    }

    public void desactivarAllHuds() {
        for(int i = 0; i < miniFactoriesA.Length; i++) {
            miniFactoriesA[i].GetComponent<CreateNewRobots>().desactivarSelectRobotHud();
        }
    }

    void putMoveAndAttack() {
        GameObject actualCasilla;
        for(int i = 0; i < casillas.Count; i++) {
            for (int j = 0; j < casillas[i].Count; j++) {
                actualCasilla = Instantiate(casillaMove);
                actualCasilla.transform.parent = casillas[i][j].transform;
                actualCasilla.transform.localPosition = new Vector3(0, 0, -1);
                actualCasilla = Instantiate(casillaAttack);
                actualCasilla.transform.parent = casillas[i][j].transform;
                actualCasilla.transform.localPosition = new Vector3(0, 0, -1);
            }
        }
        unactiveMoveAndAttack();
    }

    void unactiveMoveAndAttack() {
        for (int i = 0; i < casillas.Count; i++) {
            for (int j = 0; j < casillas[i].Count; j++) {
                casillas[i][j].transform.GetChild(0).gameObject.SetActive(false);
                casillas[i][j].transform.GetChild(1).gameObject.SetActive(false);
            }
        }
    }
    /*************************************************************************************************/
    /**
     * @brief Create template of a robot with all the information to create a replica when is needed in the game
     *
     * @param int life Total life of the robot.
     * @param int totalMove Number of boxes that te robot can move
     * @param int defense Total defense of the robot.
     * @param int resourcesNeeded Total resources necesaries to create the robot
     * @param TypeRobot headType Enum with the type of the head. The posibilities are normal, aerodynamic, ovni or tank.
     * @param TypeRobot torsoType Enum with the type of the torso. The posibilities are normal, aerodynamic, ovni or tank.
     * @param TypeRobot legsType Enum with the type of the legs. The posibilities are normal, aerodynamic, ovni or tank.
     * @param RobotWeapons weapon Enum with the weapon the robot use. The posibilities are knife, sword, gun, shotgun, machinegun, snippergun, laserweapon
     * @param int powerAttack Total power attack of the robot.
     * @param int distaceAttack  Number of boxes that te robot can attack
     * @param bool diagonalAttack If the robot can attack in diagonal
     * @param bool userRobot If the robot is created for the user or is and IA enemy.
     *  
     */

    public void createPlantilla(int life, int totalMove, int defense, int resorcesNeeded, TypeRobot headType, 
        TypeRobot torsoType, TypeRobot legsType, RobotWeapons weapon, int powerAttack, int distanceAttack, bool diagonalAttack, bool userRobot, int numRobot) {
        robot = new Robot(life, totalMove, defense, resorcesNeeded, headType,
            torsoType, legsType, weapon);

        robot.torsoType = torsoType;
        robot.legsType = legsType;
        robot.headType = headType;
        robot.weapon = weapon;
        robot.numRobotCreated = numRobot;

        //Debug.Log("Crear plantillas con ataques al elegir los robots");
        Attacks aux = new Attacks(powerAttack, distanceAttack, diagonalAttack, "attack1");
        allAttacks.Add(aux);
        robot.addAttacks(allAttacks.Count - 1);
        if (userRobot) {
            robot.blockedObjects = robotParts.objectsBlocked[(int)legsType];
            robotsAPlantilla.Add(robot);
            contadorPlantillas++;
            if(contadorPlantillas < 5) {
                GameObject parent;
                GameObject inst;
                if (torsoType == TypeRobot.AERODYNAMIC) {
                    parent = canvasSelectRobot.transform.GetChild(contadorPlantillas).transform.GetChild(0).gameObject;
                    inst = Instantiate(robotParts.UISprites[(int)torsoType][1]);
                    inst.transform.SetParent(parent.transform);
                    inst.transform.localPosition = new Vector3(0, 0, 0);
                    inst.transform.localScale = new Vector3(1, 1, 1);
                }
                else {
                    parent = canvasSelectRobot.transform.GetChild(contadorPlantillas).transform.GetChild(1).gameObject;
                    inst = Instantiate(robotParts.UISprites[(int)torsoType][1]);
                    inst.transform.SetParent(parent.transform);
                    inst.transform.localPosition = new Vector3(0, 0, 0);
                    inst.transform.localScale = new Vector3(1, 1, 1);
                }
                
                parent = canvasSelectRobot.transform.GetChild(contadorPlantillas).transform.GetChild(0).gameObject;
                inst = Instantiate(robotParts.UISprites[(int)headType][0]);
                inst.transform.SetParent(parent.transform);
                inst.transform.localPosition = new Vector3(0, 0, 0);
                inst.transform.localScale = new Vector3(1, 1, 1);

                parent = canvasSelectRobot.transform.GetChild(contadorPlantillas).transform.GetChild(2).gameObject;
                inst = Instantiate(robotParts.UISprites[(int)legsType][2]);
                inst.transform.SetParent(parent.transform);
                inst.transform.localPosition = new Vector3(0, 0, 0);
                inst.transform.localScale = new Vector3(1, 1, 1);

            }
        }
        else {
            robot.blockedObjects = robotParts.objectsBlocked[(int)legsType];
            robotsBPlantilla.Add(robot);
        }
    }


    /*************************************************************************************************/
    /**
     * @brief Return the Robot is asociated with a GameObject 
     *
     * @param GameObject robotGameObject Game object from which you want me to return you the Robot asociated
     * 
     * @returns The Robot asociated to the GameObject
     *  
     */

    public Robot returnRobot(GameObject robotGameObject) {
        Robot robObj = null;
        for(int i = 0; i < robotsA.Count; i++) {
            if(robotsA[i] != null) {
                if (robotsA[i].getRobot() == robotGameObject) {
                    robObj = robotsA[i];
                }
            }
            
        }
        for (int i = 0; i < robotsB.Count; i++) {
            if(robotsB[i] != null) { 
                if (robotsB[i].getRobot() == robotGameObject) {
                    robObj = robotsB[i];
                }
            }
        }
        return robObj;
    }

    // Update is called once per frame
    void Update() {

       //Debug.Log("TURN: " + robotTurn);
        if (test) {
            test = false;
            Robot actualRobot = null;
            for(int i = 0; i < robotsA.Count; i++) {
                if(robotsA[i].getRobot() == robotSelect) {
                    actualRobot = robotsA[i];
                }
            }
            actualRobot.attackAnim();
        }

        if (moving) {
            if (!calculado) {
                aStarScript.CalcularDistancia(casillaObjetivo, robotSelect, true, false, true, true, true);
                miniRobotsController.whatRobot(robotSelect);
                recalculatedCasilla(casillaObjetivo, robotSelect);
                casillaObjetivo.tag = ocupadaTag;
                pathWay.Clear();
                pathWay.Add(casillaObjetivo);
                //if (robotTurn == tagRobotA) {
                    for (int i = 0; i < robotSelect.GetComponent<dondeMover>().pathRobot.Count; i++) {
                        pathWay.Add(robotSelect.GetComponent<dondeMover>().pathRobot[i]);
                    }
                /*}
                else {
                    for (int i = 0; i < robotSelect.GetComponent<enemyPosition>().path.Count; i++) {
                        pathWay.Add(robotSelect.GetComponent<enemyPosition>().path[i]);
                    }
                }*/
                if(robotTurn == "robotA") {
                    returnRobot(robotSelect).walkAnim();
                }
                
                position = pathWay.Count - 1;
                calculado = true;
            }
            /*else {
                cleanCasillas();
                for (int i = 0; i < pathWay.Count; i++) {
                    pathWay[i].GetComponent<SpriteRenderer>().color = Color.blue;
                }
            }*/
           else {
                moving = false;
                if(robotTurn == tagRobotA) {
                    miniRobotsController.moveMiniRobot(pathWay[position].transform.position.x, pathWay[position].transform.position.y);
                    moveRobot(pathWay[position], robotSelect, false);
                    
                }
                else {
                    miniRobotsController.moveMiniRobot(pathWay[position].transform.position.x, pathWay[position].transform.position.y);
                    moveRobot(pathWay[position], robotSelect, true);
                }
                position--;
            }
        }

        if (tirarIA) {
            if(robotsB[totalMovido] != null) {
                tirarIA = false;
                //robotsB[totalMovido].getRobot().GetComponent<enemyPosition>().turnIA();
                GameObject temp = robotsB[totalMovido].getRobot();
                Debug.Log(temp.name);
                temp.GetComponent<enemyPosition>().newIA();
            }
            else {
                totalMovido++;
                if(totalMovido < robotsB.Count) {
                    tirarIA = true;
                }
                else {
                    pasarTurno();
                }

            }
               
            //robotsB[totalMovido].getRobot().GetComponent<enemyPosition>().newIA();

            //IAController.newIA();

        }
    }

    /*************************************************************************************************/
    /**
     * @brief Check if is the turn of a selected robot. Check the tag of the game object param. If is
     * equal to robotTurn return true, else return false.
     *
     * @param GameObject robot
     * 
     * @returns the result of the operation, being cd_Error_NoError if all worked OK
     */

    public bool isTurn(GameObject robot) {
        bool turn = false;
        if (!selected && robot.tag == robotTurn) {
            robotSelect = robot;
            selected = true;
            turn = true;
        }
        return turn;
    }

    /*************************************************************************************************/
    /**
     * @brief Check if is the turn of a selected robot. Check the tag of the game object param. If is
     * equal to robotTurn return true, else return false.
     *
     * @param bool true if the robot attack false if the robot move to another square
     */

    public void selectRobot(bool attack) {
        Debug.Log("A veces llego y selected es true cuando no deberia serlo");
        if (!selected && robotSelect.tag == robotTurn) {
            if (!attack && !robotSelect.GetComponent<dondeMover>().movido) {
                robotSelect.GetComponent<dondeMover>().printMove(true, false);
            }
            else if (!robotSelect.GetComponent<dondeMover>().atacado && attack) {
                robotSelect.GetComponent<dondeMover>().printMove(true, true);
            }
            selected = true;
        }
        else if (selected && robotSelect.tag == robotTurn) {
            cleanCasillas();
            selected = false;
            atacando = false;
        }
    }

    /*************************************************************************************************/
    /**
     * @brief If the square you press has the tag move the robot move to this square
     *
     * @param GameObject casilla: the square you clicked
     * @param GameObject robotSelected: the robot you have select
     
     */

    public void moveRobot(GameObject casilla, GameObject robotSelected, bool atacar) {
        //if (casilla.tag == moveTag) {
        if (!calculated) {
            moveX = (casilla.transform.position.x - robotSelected.transform.position.x) / stepsToObjective;
            moveY = (casilla.transform.position.y - robotSelected.transform.position.y) / stepsToObjective;
            calculated = true;
        }
        while(!inPlaceY) {
            if (!inPlaceX) {
                if (contadorMoving > maxCount) {
                    if (times < stepsToObjective) {
                        robotSelected.transform.position = new Vector3(robotSelected.transform.position.x + moveX, robotSelected.transform.position.y, robotSelected.transform.position.z);
                        times++;
                        contadorMoving = 0;
                    }
                    else {
                        inPlaceX = true;
                        times = 0;
                    }
                }
                else {
                    //Debug.Log("Contador subiendo: " + contadorMoving);
                    contadorMoving += Time.deltaTime;
                }
            }
            else if (!inPlaceY) {
                if (contadorMoving > maxCount) {
                    if (times < stepsToObjective) {
                        robotSelected.transform.position = new Vector3(robotSelected.transform.position.x, robotSelected.transform.position.y + moveY, robotSelected.transform.position.z);
                        times++;
                        contadorMoving = 0;
                    }
                    else {
                        inPlaceY = true;
                    }
                }
                else {
                    contadorMoving += Time.deltaTime;
                }
            }
        }
        
        moving = true;
        calculated = false;
        inPlaceX = false;
        inPlaceY = false;
        times = 0;
        if (position == 0) {
            if(robotTurn == "robotA") {
                returnRobot(robotSelect).idleAnim();
            }
            
            cleanCasillas();
            recalculateOcupadas();
            robotSelect.GetComponent<dondeMover>().moveImage.SetActive(false);
            //ATACK HERE
            robotSelect.GetComponent<dondeMover>().movido = true;
            selected = false;
            moving = false;
            calculated = false;
            calculado = false;
            inPlaceX = false;
            inPlaceY = false;
            times = 0;
            if (atacar) {
                robotSelect.GetComponent<enemyPosition>().atacadoAlEnemigo();
            }
        }
    }
    

    /*************************************************************************************************/
    /**
     * @brief Clean the color of all the squares and return to white
    
     */
    
    public void cleanCasillas() {
        unactiveMoveAndAttack();
        bool untaggear = true;
        for (int i = 0; i < casillas.Count; i++) {
            for (int j = 0; j < casillas[i].Count; j++) {
                casillas[i][j].GetComponent<SpriteRenderer>().color = Color.white;
                for(int k = 0; k < badTags.Count; k++) {
                    //Debug.Log(casillas[i][j].tag + " == " + badTags[k]);
                    if(casillas[i][j].tag == badTags[k]) {
                        untaggear = false;
                        //casillas[i][j].tag = noTag;
                    }
                }
                if (untaggear && casillas[i][j].tag != "ocupada") {
                    casillas[i][j].tag = noTag;
                }
                else {
                    untaggear = true;
                }
            }
        }
        for(int i = 0; i < FactoriesA.Count; i++) {
            for(int j = 0; j < FactoriesA[i].GetComponent<FactoryManager>().positions.Count; j++) {
                FactoriesA[i].GetComponent<FactoryManager>().positions[j].tag = "factory";
            }
        }
        for (int i = 0; i < FactoriesB.Count; i++) {
            for (int j = 0; j < FactoriesB[i].GetComponent<FactoryManager>().positions.Count; j++) {
                FactoriesB[i].GetComponent<FactoryManager>().positions[j].tag = "factory";
            }
        }
        for(int i = 0; i < water.Length; i++) {
            water[i].tag = "water";
        }
    }

    /*************************************************************************************************/
    /**
     * @brief Calculate the square where there is the robot after move
     *
     * @param GameObject casillaObjetivo Squere whete the robot move. 
     * @param GameObject robot The robot moved.
     
     */

    public void recalculatedCasilla(GameObject casillaObjetivo, GameObject robot) {

        casillas[robot.GetComponent<dondeMover>().numFila][robot.GetComponent<dondeMover>().numCol].tag = noTag;
        for (int i = 0; i < casillas.Count; i++) {
            for (int j = 0; j < casillas[i].Count; j++) {
                if (casillas[i][j] == casillaObjetivo) {
                    robot.GetComponent<dondeMover>().numFila = i;
                    robot.GetComponent<dondeMover>().numCol = j;
                }
            }
        }
        robot.GetComponent<dondeMover>().movido = true;
        for(int i = 0; i < robotsA.Count; i++) {
            if(robotsA[i] != null) { 
                if(robot.GetComponent<dondeMover>().myId == robotsA[i].getId()) {
                    robotsA[i].setCol(robot.GetComponent<dondeMover>().numCol);
                    robotsA[i].setRow(robot.GetComponent<dondeMover>().numFila);
                }
            }
        }

        for (int i = 0; i < robotsB.Count; i++) {
            if(robotsB[i] != null) { 
                if (robot.GetComponent<dondeMover>().myId == robotsB[i].getId()) {
                    if (robotsB[i] != null) {
                        robotsB[i].setCol(robot.GetComponent<dondeMover>().numCol);
                        robotsB[i].setRow(robot.GetComponent<dondeMover>().numFila);
                    }
                }
            }
        }
    }


    /*************************************************************************************************/
    /**
     * @brief After attack check if all the robots have the bool attack in true to check if it must pass 
     *   turn.
     *
     * @returns Return a boolean. Return true if all the robots has attack. 
     */

    public bool checkAtacado() {
        bool allMove = true;
        if (robotTurn == tagRobotA) {
            for (int i = 0; i < robotsA.Count; i++) {
                if (!robotsA[i].getRobot().GetComponent<dondeMover>().atacado) {
                    allMove = false;
                }
            }
        }

        else if (robotTurn == tagRobotB) {
            for (int i = 0; i < robotsB.Count; i++) {
                if(robotsB[i] != null) { 
                    if(robotsB[i].getRobot() != null) {
                        if (!robotsB[i].getRobot().GetComponent<dondeMover>().atacado) {
                            allMove = false;
                        }
                    }
                }
            }
        }

        if (allMove) {
            pasarTurno();
            recalculateOcupadas();
        }
        return allMove;
    }


    /*************************************************************************************************/
    /**
     * @brief Pass the turn.

     */

    private void recoverIcons() {
        for(int i = 0; i < robotsA.Count; i++) {
            if(robotsA[i] != null) {
                robotsA[i].getRobot().GetComponent<dondeMover>().attackImage.SetActive(true);
                robotsA[i].getRobot().GetComponent<dondeMover>().moveImage.SetActive(true);
            }
        }
        for (int i = 0; i < robotsB.Count; i++) {
            if (robotsB[i] != null) {
                robotsB[i].getRobot().GetComponent<dondeMover>().attackImage.SetActive(true);
                robotsB[i].getRobot().GetComponent<dondeMover>().moveImage.SetActive(true);
            }
        }
    }

    /*************************************************************************************************/
    /**
     * @brief Funtion called when you what to pass the turn
     *
     */

    public void pasarTurno() {
        if (checkGameOverB()) {
            Debug.Log("El ordenador a perdido");
            finalTime = Time.time - initialTime;
            Metrics.totalTime = finalTime;
            Debug.Log(finalTime);
            SceneManager.LoadScene("MetricsScene");
        }
        for (int i = 0; i < factories.Count; i++) {
            if (factories[i].GetComponent<FactoryManager>().robotType == robotTurn) {
                if(robotTurn == "robotB") {
                    factories[i].GetComponent<FactoryManager>().createResources(true);
                }
                else {
                    factories[i].GetComponent<FactoryManager>().createResources(true);
                }
            }
        }

        if(!checkGameOverB()) {
            if (robotTurn == tagRobotA) {
                for (int i = 0; i < robotsB.Count; i++) {
                    if(robotsB[i] != null) {
                        if (robotsB[i].getRobot() != null) {
                            robotsB[i].getRobot().GetComponent<dondeMover>().atacado = false;
                            robotsB[i].getRobot().GetComponent<dondeMover>().movido = false;
                            selected = false;
                        }
                    }
                    
                }
                totalMovido = 0;
                tirarIA = true;
                Metrics.totalTurnsA++;
                selected = false;
                recoverIcons();
                robotTurn = tagRobotB;
            }
            else {
                for (int i = 0; i < robotsA.Count; i++) {
                    if(robotsA[i] != null) {
                        if(robotsA[i].getRobot() != null) {
                            robotsA[i].getRobot().GetComponent<dondeMover>().movido = false;
                            robotsA[i].getRobot().GetComponent<dondeMover>().atacado = false;
                            selected = false;
                        }
                        
                    }
                    
                }
                Metrics.totalTurnsB++;
                selected = false;
                recoverIcons();
                robotTurn = tagRobotA;
            }
            cameraMovement.moveCamera = true;
            cameraMovement.changeSprite();
        }
        actionHUD.SetActive(false);
        cleanCasillas();
        //sendRobotTurn(robotTurn);
    }

    private bool checkGameOverB() {
        bool noRobots = true;
        for(int i = 0; i < robotsB.Count; i++) {
            if(robotsB[i] != null) {
                noRobots = false;
            }
        }
        return noRobots;
    }

    /*************************************************************************************************/
    /**
     * @brief Has the robot selected attack.
     *
     * @param GameObject robotObjetivo. The robot that the robotSelected has attack
     *
     */

    float returndebility(RobotWeapons weapon, Robot robotAttacked) {
        float debility = 0;
        if(weapon == RobotWeapons.GUN) {
            debility = robotAttacked.debilityGun;
        }
        else if (weapon == RobotWeapons.KNIFE) {
            debility = robotAttacked.debilityKnife;
        }
        else if (weapon == RobotWeapons.LASERWEAPON) {
            debility = robotAttacked.debilityLaser;
        }
        else if (weapon == RobotWeapons.MACHINEGUN) {
            debility = robotAttacked.debilityMachineGun;
        }
        else if (weapon == RobotWeapons.SHOTGUN) {
            debility = robotAttacked.debilityShotgun;
        }
        else if (weapon == RobotWeapons.SNIPPERGUN) {
            debility = robotAttacked.debilitySniper;
        }
        else if (weapon == RobotWeapons.SWORD) {
            debility = robotAttacked.debilitySword;
        }
        return debility;
    }
    
    /*************************************************************************************************/
    /**
     * @brief Funtion called when the robot attack
     *
     * @param GameObject robotObjetivo The robot that the robotSelected has attack
     * @param RobotWeapons weaponAttack The type of the weapon of the robot has attack to calculate the debility of 
     * the robotObjetivo.
     *
     */

    public void atacar(GameObject robotObjetivo, RobotWeapons weaponAttack) {
        bool distanceRestriction = false;
        float totalDebility = 0;
        if (robotObjetivo != null) {
            if(robotObjetivo.tag == "robotA" || robotObjetivo.tag == "robotB") {
                totalDebility = returndebility(weaponAttack, returnRobot(robotObjetivo));
            }
        }
        
        if (weaponAttack == RobotWeapons.SHOTGUN || weaponAttack == RobotWeapons.MACHINEGUN || weaponAttack == RobotWeapons.GUN) {
            distanceRestriction = true;
        }

        if (distanceRestriction && robotObjetivo != null) {
            dondeMover DondeMover = robotSelect.GetComponent<dondeMover>();
            dondeMover DondeMover2 = robotObjetivo.GetComponent<dondeMover>();
            manhattanDistance = Math.Abs(DondeMover.numFila - DondeMover2.numFila) +
                Math.Abs(DondeMover.numCol - DondeMover2.numCol);
            Debug.Log("Distance Manhattan: " + manhattanDistance);
            if (DondeMover.numFila != DondeMover2.numFila &&
                DondeMover.numCol != DondeMover2.numCol) {
                manhattanDistance--;
            }
            attackRestriction = (manhattanDistance - 1) * 10;
        }

        else {
            attackRestriction = 0;
        }
        
        //Chequeamos que uno de los dos robots este atacando
        if (atacando && robotObjetivo != null || robotTurn == tagRobotB && robotObjetivo != null) {
            if (robotObjetivo.tag != robotTurn) {
                if (casillas[robotObjetivo.GetComponent<dondeMover>().numFila][robotObjetivo.GetComponent<dondeMover>().numCol].tag == "attack") {
                    Debug.Log("Attack Power robotSelect is: " + robotSelect.GetComponent<dondeMover>().attackPower);
                    Debug.Log("Attack Power of robotObjetivo is: " + robotObjetivo.GetComponent<dondeMover>().attackPower);
                    robotObjetivo.GetComponent<dondeMover>().life -= (robotSelect.GetComponent<dondeMover>().attackPower + totalDebility) - attackRestriction;
                    //sendRobotLife(robotObjetivo.GetComponent<dondeMover>().life, robotObjetivo);

                    if(weaponAttack == RobotWeapons.KNIFE) {
                        musicController.audioSource[1].Stop();
                        musicController.audioSource[1].clip = musicController.Music.audioClips[2];
                        musicController.audioSource[1].Play();
                    }
                    else if (weaponAttack == RobotWeapons.SWORD || weaponAttack == RobotWeapons.LASERWEAPON) {
                        musicController.audioSource[1].Stop();
                        musicController.audioSource[1].clip = musicController.Music.audioClips[3];
                        musicController.audioSource[1].Play();
                    }
                    else if (weaponAttack == RobotWeapons.MACHINEGUN || weaponAttack == RobotWeapons.SNIPPERGUN) {
                        musicController.audioSource[1].Stop();
                        musicController.audioSource[1].clip = musicController.Music.audioClips[4];
                        musicController.audioSource[1].Play();
                    }
                    else if (weaponAttack == RobotWeapons.GUN) {
                        musicController.audioSource[1].Stop();
                        musicController.audioSource[1].clip = musicController.Music.audioClips[5];
                        musicController.audioSource[1].Play();
                    }
                    else if (weaponAttack == RobotWeapons.SHOTGUN) {
                        musicController.audioSource[1].Stop();
                        musicController.audioSource[1].clip = musicController.Music.audioClips[7];
                        musicController.audioSource[1].Play();
                    }

                    robotExplote = robotObjetivo;
                    ActivateExplosion(weaponAttack);
                    robotSelect.GetComponent<dondeMover>().attackImage.SetActive(false);
                    actualLife = robotObjetivo.GetComponent<dondeMover>().life / robotObjetivo.GetComponent<dondeMover>().totalLife;
                    robotObjetivo.transform.GetChild(0).transform.GetChild(1).transform.localScale =
                        new Vector3(actualLife, robotObjetivo.transform.GetChild(0).transform.GetChild(1).transform.localScale.y, robotObjetivo.transform.GetChild(0).transform.GetChild(1).transform.localScale.z);
                    if (robotObjetivo.GetComponent<dondeMover>().life <= 0) {
                        musicController.audioSource[1].Stop();
                        musicController.audioSource[1].clip = musicController.Music.audioClips[8];
                        musicController.audioSource[1].Play();
                        removeRobot(robotObjetivo);
                        Destroy(miniRobots[GameObjectToMiniRobotPos(robotObjetivo)]);
                        Destroy(robotObjetivo);
                        destruido = true;
                    }
                    //Recojo metricas
                    if (robotTurn == tagRobotA) {
                        robotSelect.transform.GetChild(3).GetComponent<AnimationController>().attack = true;
                        robotSelect.transform.GetChild(3).GetComponent<AnimationController>().robot = returnRobot(robotSelect);
                        returnRobot(robotSelect).attackAnim();
                        Metrics.numAttacksA++;
                        Metrics.totalPowerAttackA += (robotSelect.GetComponent<dondeMover>().attackPower + (int)totalDebility) - attackRestriction;
                        Metrics.totalLostLiveB += (robotSelect.GetComponent<dondeMover>().attackPower + (int)totalDebility) - attackRestriction;
                        if (robotObjetivo.GetComponent<dondeMover>().life <= 0) {
                            Metrics.robotsLostB++;
                            Metrics.robotsDestroyedA++;
                        }
                    }
                    else {
                        Metrics.numAttacksB++;
                        Metrics.totalPowerAttackB += (robotSelect.GetComponent<dondeMover>().attackPower + (int)totalDebility) - attackRestriction;
                        Metrics.totalLostLiveA += (robotSelect.GetComponent<dondeMover>().attackPower + (int)totalDebility) - attackRestriction;
                        if (robotObjetivo.GetComponent<dondeMover>().life <= 0) {
                            Metrics.robotsLostA++;
                            Metrics.robotsDestroyedB++;
                        }
                    }
                    cleanCasillas();
                    recalculateOcupadas();
                    atacando = false;
                    robotSelect.GetComponent<dondeMover>().atacado = true;
                    if (online == false && robotTurn == tagRobotA) {
                        checkAtacado();
                    }
                    selected = false;
                }

                else {
                    Debug.Log("NO PUEDES ATACAR");
                }
            }
        }
        else {
            //Debug.Log("Ningun robot esta atacando en este momento");
        }
    }

    void DeactivateExplosions() {
        if (robotExplote != null) {
            GameObject explosion = robotExplote.transform.GetChild(3).GetChild(0).gameObject;

            for (int i = 0; i < explosion.transform.childCount; i++) {
                explosion.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
           
    }

    void ActivateExplosion(RobotWeapons weapon) {
        Debug.Log(weapon);
        GameObject explosion = robotExplote.transform.GetChild(3).GetChild(0).gameObject;
        explosion.transform.GetChild((int)weapon).gameObject.SetActive(true);
        Invoke("DeactivateExplosions", 1f);
    }

    public void recalculateOcupadas() {
        for(int i = 0; i < robotsB.Count; i++) {
            if(robotsB[i] != null) {
                casillas[robotsB[i].getRobot().GetComponent<dondeMover>().numFila][robotsB[i].getRobot().GetComponent<dondeMover>().numCol].tag = "ocupada";
            }
        }
            
    }

    /*************************************************************************************************/
    /**
     * @brief Funtion that return the position of the miniRobot in miniRobots array
     *
     * @param GameObject robotDelete The robot that is doing to delete
     *
     * @returns The position of the miniRobot in miniRobots array
     */

    public int GameObjectToMiniRobotPos(GameObject robotDelete) {
        int returnID = 0;
        for(int i = 0; i < miniRobots.Count; i++) {
            if(miniRobots[i] != null) {
                if(miniRobots[i].GetComponent<miniRobotsID>().myID == robotDelete.GetComponent<dondeMover>().miniRobot) {
                    returnID = i;
                }
            }
        }
        return returnID;
    }

    /*************************************************************************************************/
    /**
     * @brief Return the total robots of the player to check the GameOver.
     * 
     * @returns Return the total robots of the player
     */

    public int totalRobots() {
        int total = 0;
        for(int i = 0; i > robotsA.Count; i++) {
            if(robotsA[i] != null) {
                total++;
            }
        }
        return total;
    }

    void removeRobot(GameObject robot) {
        if(robotTurn == "robotB") {
            for (int i = 0; i < robotsA.Count; i++) {
                if(robotsA[i] != null) {
                    if(robotsA[i].getRobot() != null) {
                        if (robotsA[i].getRobot() == robot) {
                            robotsA[i] = null;
                        }
                    }
                }
            }
            reorder();
        }

        else {
            for (int i = 0; i < robotsB.Count; i++) {
                if (robotsB[i] != null) {
                    if (robotsB[i].getRobot() != null) {
                        if (robotsB[i].getRobot() == robot) {
                            robotsB[i] = null;
                        }
                    }
                }
            }
        }
    }

    void reorder() {
        if(robotTurn == "robotB") {
            for(int i = 0; i < robotsA.Count; i++) {
                if(robotsA[i] != null) {
                    temporalList.Add(robotsA[i]);
                }
            }
            robotsA.Clear();
            robotsA = temporalList;
            temporalList.Clear();
        }

        else {
            for (int i = 0; i < robotsB.Count; i++) {
                if (robotsB[i] != null) {
                    temporalList.Add(robotsB[i]);
                }
            }
            robotsB.Clear();
            robotsB = temporalList;
            temporalList.Clear();
        }
    }

    /*************************************************************************************************/
    /**
     * @brief Called when attack the factories
     * 
     * @param GameObject factory that the robot has attack
     */

    public void attackFactory(GameObject factory) {
        //bool attack = false;
        for (int i = 0; i < factory.GetComponent<FactoryManager>().positions.Count; i++) {
            if(factory.GetComponent<FactoryManager>().positions[i].tag == "attack") {
                factory.GetComponent<FactoryManager>().life -= robotSelect.GetComponent<dondeMover>().attackPower;
                Metrics.totalPowerAttackB += robotSelect.GetComponent<dondeMover>().attackPower;
                robotSelect.GetComponent<dondeMover>().attackImage.SetActive(false);
                if (factory.GetComponent<FactoryManager>().life < 0) {
                    factory.GetComponent<FactoryManager>().life = 0;
                    factory.GetComponent<FactoryManager>().death = true;
                }
                actualLife = factory.GetComponent<FactoryManager>().life / factory.GetComponent<FactoryManager>().maxLife;
                factory.transform.GetChild(0).transform.GetChild(1).transform.localScale =
                        new Vector3(actualLife,
                            factory.transform.GetChild(0).transform.GetChild(1).transform.localScale.y,
                            factory.transform.GetChild(0).transform.GetChild(1).transform.localScale.z);
                if(actualLife <= 0) {
                    Destroy(factory);
                    if (chechkGameOverA(factory)) {
                        Debug.Log("El jugador a perdido");
                        finalTime = Time.time - initialTime;
                        Metrics.totalTime = finalTime;
                        Debug.Log(finalTime);
                        SceneManager.LoadScene("MetricsScene");
                    }
                    else {
                        Debug.Log(FactoriesA[0].name);
                        Debug.Log(GameObject.FindGameObjectsWithTag("factoryA").Length);
                    }
                }
                cleanCasillas();
                actionHUD.SetActive(false);
            }
        }
    }

    bool chechkGameOverA(GameObject factoryDeleted) {
        bool GameOver = true;
        int count = 0;
        for(int i = 0; i < FactoriesA.Count; i++) {
            if (FactoriesA[i] != null) {
                if (FactoriesA[i] != factoryDeleted) {
                    count++;
                }
            }
        }
        if(count > 0) {
            GameOver = false;
        }
        return GameOver;
    }

    void instantiateInitRobots() {
        //Esta funcion se quitara 
        for (int i = 0; i > robotsA.Count; i++) {
            GameObject newRobot = instantiateRobots(robotsA[i].getRow(), robotsA[i].getCol(), robotsA[i].getMove(), false, robotsA[i].getLife(), 
                allAttacks[robotsA[i].getAttacks()[0]].getPowerAttack(), allAttacks[robotsA[i].getAttacks()[0]].getDistanceAttack(), robotsA[i].blockedObjects);
            //robotsA[i].setRobot(newRobot);
        }
    }

    /*************************************************************************************************/
    /**
     * @brief Instantiate a robot in the position you especificate
     *
     * @param int row Row of the square in the battle field.
     * @param int col Col of the square in the battle field.
     *
     * @returns return the GameObject instantiated
     */

    GameObject instantiateRobots(int row, int col, int move, bool enemy, int totalLife, int PowerAttack, int numAttack, List<string> blockObj) {
        Vector3 initPosition = casillas[col][row].transform.position;
        initPosition.z = -1;
        GameObject myRobot = null;
        if (enemy) {
            if (!online){
                myRobot =
                Instantiate(Resources.Load("Prefabs/RobotBase"),
                initPosition,
                Quaternion.identity) as GameObject;
                myRobot.tag = "robotB";
            }
        }
        else {
            myRobot =
            Instantiate(Resources.Load("Prefabs/RobotBase"),
            initPosition,
            Quaternion.identity) as GameObject;
            myRobot.tag = "robotA";
        }
        myRobot.GetComponent<dondeMover>().miniRobot = nextMinirobotID;
        myRobot.GetComponent<dondeMover>().numFila = col;
        myRobot.GetComponent<dondeMover>().numCol = row;
        myRobot.GetComponent<dondeMover>().numCasillas = move;
        myRobot.GetComponent<dondeMover>().totalLife = totalLife;
        myRobot.GetComponent<dondeMover>().life = totalLife;
        myRobot.GetComponent<dondeMover>().attackPower = PowerAttack;
        myRobot.GetComponent<dondeMover>().numAttack = numAttack;
        myRobot.GetComponent<dondeMover>().objectsBlocked = blockObj;
        return myRobot;
    }

    /*************************************************************************************************/
    /**
     * @brief Crete a new robot equal to the robot of the template.
     * 
     * @param numRobot position of the template in the array
     */

    public void createNewRobot(int numRobot) {
        canvasSelectRobot.SetActive(false);
        cameraMovement.moveCamera = true;
        cameraMovement.changeSprite();
        selectedBox = selectedFactory.GetComponent<CreateNewRobots>().selectBox();
        if(selectedBox != null) {
            int col = (int)selectedBox.transform.position.y;
            int row = (int)selectedBox.transform.position.x;
            if (robotTurn == "robotA") {
                Metrics.spawnedUnitiesA++;
                for(int i = 0; i < FactoriesA.Count; i++) {
                    if(FactoriesA[i] != null) {
                        FactoriesA[i].GetComponent<FactoryManager>().actualResources -= robotsAPlantilla[numRobot].getResources();
                        RM.renewResources(FactoriesA[i].GetComponent<FactoryManager>().topResources, FactoriesA[i].GetComponent<FactoryManager>().actualResources);
                    }
                    
                }
                createAndInstantiate(robotsAPlantilla[numRobot], col, row, robotsAPlantilla[numRobot].getMove(), false);
                miniRobotActual = miniRobotsController.instantiateMiniRobot(false, row, col);
                miniRobots.Add(miniRobotActual);
                casillas[col][row].tag = "ocupada";
                //robotsA.Add
            }
            else {
                Metrics.spawnedUnitiesB++;
                for (int i = 0; i < FactoriesB.Count; i++) {
                    if (FactoriesB[i] != null) {
                        FactoriesB[i].GetComponent<FactoryManager>().actualResources -= robotsBPlantilla[numRobot].getResources();
                        FactoriesB[i].GetComponent<FactoryManager>().createResources(false);
                        //RM.renewResources(FactoriesB[i].GetComponent<FactoryManager>().topResources, FactoriesB[i].GetComponent<FactoryManager>().actualResources);
                        resourcesB = FactoriesB[i].GetComponent<FactoryManager>().actualResources;
                    }
                }
                createAndInstantiate(robotsBPlantilla[numRobot], col, row, robotsBPlantilla[numRobot].getMove(), true);
                casillas[col][row].tag = "ocupada";
            }
        }
        else {
            Debug.Log("No hay espacio para crear nuevos robots.");
        }   
    }

    private Robot initNewRobot(Robot plantilla, bool enemy) { 
        Robot robot = new Robot(plantilla.getLife(), plantilla.getMove(), plantilla.getDefense(), plantilla.getResources());
                
        robot.initRobotParts(plantilla.getHeadType(), plantilla.torsoType, plantilla.legsType, plantilla.weapon, plantilla.numRobotCreated);
        
        for (int i = 0; i < plantilla.getAttacks().Count; i++) {
            robot.addAttacks(plantilla.getAttacks()[i]);
        }

        robot.headType = plantilla.headType;
        robot.torsoType = plantilla.torsoType;
        robot.legsType = plantilla.legsType;
        robot.weapon = plantilla.weapon;

        return robot;
    }

    /*************************************************************************************************/
    /**
     * @brief Instantiate a new robot with the template pass for parameter and in the position especificated
     * 
     * @param col Y position of the robot
     * @param row X position of the robot
     * @param move totalMove of the robot
     * @param enemy If the robot instantiated is enemy or not
     * 
     */

    public void createAndInstantiate(Robot plantilla, int col, int row, int move, bool enemy) {
        Robot newRobot = initNewRobot(plantilla, enemy);
        if (enemy) {
            robotsB.Add(newRobot);
            if (online) {
                newRobot.setRow(row);
                newRobot.setCol(col);
                newRobot.setMove(move);
            }
        }
        else {
            robotsA.Add(newRobot);
            if (online) {
                newRobot.setRow(row);
                newRobot.setCol(col);
                newRobot.setMove(move);
            }
        }
        if (!online) {
            if (enemy) {
                robotsB[robotsB.Count - 1].setRobot(instantiateRobots(row, col, move, enemy, newRobot.getLife(), 
                    allAttacks[newRobot.getAttacks()[0]].getPowerAttack(), allAttacks[newRobot.getAttacks()[0]].getDistanceAttack(), newRobot.blockedObjects));
                GameObject parent = robotsB[robotsB.Count - 1].getRobot();
                parent.AddComponent<enemyPosition>();
                for (int i = 0; i < robotsB[robotsB.Count - 1].parts.Count; i++) {
                    robotsB[robotsB.Count - 1].getRobot().transform.position = new Vector3(0, 0, 0);
                    GameObject part = Instantiate(robotsB[robotsB.Count - 1].parts[i]);
                    part.transform.parent = parent.transform;
                    if (i == 0) {
                        parent = part;
                    }
                    robotsB[robotsB.Count - 1].getRobot().transform.position = new Vector3(row, col, -1);
                    /*if (i == 0) {
                        thisRobot = part;
                    }*/
                }
                robotsB[robotsB.Count - 1].idleAnim();
                robotsB[robotsB.Count - 1].getRobot().GetComponent<dondeMover>().miniRobot = nextMinirobotID;
            }
            else {
                robotsA[robotsA.Count - 1].setRobot(instantiateRobots(row, col, move, enemy, newRobot.getLife(),
                    allAttacks[newRobot.getAttacks()[0]].getPowerAttack(), allAttacks[newRobot.getAttacks()[0]].getDistanceAttack(), newRobot.blockedObjects));
                GameObject parent = robotsA[robotsA.Count - 1].getRobot();
                for (int i = 0; i < robotsA[robotsA.Count - 1].parts.Count; i++) {
                    robotsA[robotsA.Count - 1].getRobot().transform.position = new Vector3(0, 0, 0);
                    GameObject part = Instantiate(robotsA[robotsA.Count - 1].parts[i]);
                    part.transform.parent = parent.transform;
                    if(i == 0) {
                        parent = part;
                    }
                    robotsA[robotsA.Count - 1].getRobot().transform.position = new Vector3(row, col, -1);
                    /*if (i == 0) {
                        thisRobot = part;
                    }*/
                }
                robotsA[robotsA.Count - 1].idleAnim();
                robotsA[robotsA.Count - 1].getRobot().GetComponent<dondeMover>().miniRobot = nextMinirobotID;
            }
        }
    }

    /*************************************************************************************************/
    /**
     * @brief Put the caracteristics of the robot for use in the future
     * 
     * @param GameObject robot The GameObject that instantiated
     * @param col Y position of the robot
     * @param row X position of the robot
     * @param move Total move of the robot
     * @param Robot The actual robot created
     * 
     */

    public void initRobot(GameObject robot, int col, int row, int move, Robot actualRobot) {
        robot.GetComponent<dondeMover>().numFila = col;
        robot.GetComponent<dondeMover>().numCol = row;
        robot.GetComponent<dondeMover>().numCasillas = move;
        robot.GetComponent<dondeMover>().attackPower = actualRobot.getAttacks()[0];
        Debug.Log("Actual power attack is: " + robot.GetComponent<dondeMover>().attackPower);
        actualRobot.setCol(row);
        actualRobot.setRow(col);
        actualRobot.setMove(move);
    }

    public void OnEvent(EventData photonEvent) {
        if (photonEvent.Code == 5) {
            if (!sended) {
                string aux = (string)photonEvent.CustomData;
                robot = new Robot(aux);
                for (int i = 0; i < GameObject.FindGameObjectsWithTag("robotB").Length; i++) {
                    int x = (int)GameObject.FindGameObjectsWithTag("robotB")[i].transform.position.x;
                    int y = (int)GameObject.FindGameObjectsWithTag("robotB")[i].transform.position.y;
                    if (robot.getCol() == (x * boxSize) && robot.getRow() == (y * boxSize)) {
                        auxGameObj = GameObject.FindGameObjectsWithTag("robotB")[i];
                        //robot.setRobot(auxGameObj);
                        //auxGameObj.GetComponent<dondeMover>().newId();
                    }
                }
                //Cambiando variables en dondeMover
                auxGameObj.GetComponent<dondeMover>().numCol = robot.getCol();
                auxGameObj.GetComponent<dondeMover>().numFila = robot.getRow();
                auxGameObj.GetComponent<dondeMover>().numCasillas = robot.getMove();
                if (PhotonNetwork.IsMasterClient) {
                    robotsB.Add(robot);
                    if (!sended) {
                        sendInformation(robotsA[robotsA.Count - 1].getDataSend());
                        sended = true;
                    }
                }
            }

            else {
                sended = false;
            }
        }

        if (photonEvent.Code == 6) {
            if (!sended) {
                string aux = (string)photonEvent.CustomData;
                robot = new Robot(aux);
                for (int i = 0; i < GameObject.FindGameObjectsWithTag("robotA").Length; i++) {
                    int x = (int)GameObject.FindGameObjectsWithTag("robotA")[i].transform.position.x;
                    int y = (int)GameObject.FindGameObjectsWithTag("robotA")[i].transform.position.y;
                    if (robot.getCol() == (x * boxSize) && robot.getRow() == (y * boxSize)) {
                        auxGameObj = GameObject.FindGameObjectsWithTag("robotA")[i];
                        //robot.setRobot(auxGameObj);
                    }
                }
                //Cambiando variables en dondeMover
                auxGameObj.GetComponent<dondeMover>().numCol = robot.getCol();
                auxGameObj.GetComponent<dondeMover>().numFila = robot.getRow();
                auxGameObj.GetComponent<dondeMover>().numCasillas = robot.getMove();
                if (!PhotonNetwork.IsMasterClient) {
                    robotsA.Add(robot);
                }
                sended = true;
            }
            else {
                sended = false;
            }
        }

        if (photonEvent.Code == 7) {
            string aux = (string)photonEvent.CustomData;
            robotTurn = aux;
            if (robotTurn == "robotA")  {
                for (int i = 0; i < robotsA.Count; i++)  {
                    if (robotsA[i] != null) {
                        robotsA[i].getRobot().GetComponent<dondeMover>().movido = false;
                        robotsA[i].getRobot().GetComponent<dondeMover>().atacado = false;
                    }
                }
            }
            else  {
                for (int i = 0; i < robotsB.Count; i++)  {
                    if (robotsB[i] != null)  {
                        robotsB[i].getRobot().GetComponent<dondeMover>().movido = false;
                        robotsB[i].getRobot().GetComponent<dondeMover>().atacado = false;
                    }
                }
            }
        }

        if (photonEvent.Code == 8) {
            object[] inf = new object[2];
            inf = (object[])photonEvent.CustomData;
            Debug.Log("Life: " + inf[0] + "ID: " + inf[1]);
            bool aux = false;
           
            for(int i = 0; i < GameObject.FindGameObjectsWithTag("robotA").Length; i++) {
                Debug.Log("myId is: " + GameObject.FindGameObjectsWithTag("robotA")[i].GetComponent<dondeMover>().myId + " " + (int)inf[1]);
                if (GameObject.FindGameObjectsWithTag("robotA")[i].GetComponent<dondeMover>().myId == (int)inf[1]) {
                    GameObject robotObjetivo = GameObject.FindGameObjectsWithTag("robotA")[i];
                    robotObjetivo.GetComponent<dondeMover>().life = (float)inf[0];
                    actualLife = robotObjetivo.GetComponent<dondeMover>().life / robotObjetivo.GetComponent<dondeMover>().totalLife;
                    robotObjetivo.transform.GetChild(0).transform.GetChild(1).transform.localScale =
                       new Vector3(actualLife, robotObjetivo.transform.GetChild(0).transform.GetChild(1).transform.localScale.y, 
                       robotObjetivo.transform.GetChild(0).transform.GetChild(1).transform.localScale.z);
                    aux = true;
                }
               
            }
            if (!aux) {
                for (int i = 0; i < GameObject.FindGameObjectsWithTag("robotB").Length; i++) {
                    Debug.Log("myId is: " + GameObject.FindGameObjectsWithTag("robotA")[i].GetComponent<dondeMover>().myId + " " + (int)inf[1]);
                    if (GameObject.FindGameObjectsWithTag("robotB")[i].GetComponent<dondeMover>().myId == (int)inf[1]) {
                        GameObject robotObjetivo = GameObject.FindGameObjectsWithTag("robotB")[i];
                        robotObjetivo.GetComponent<dondeMover>().life = (float)inf[0];
                        actualLife = robotObjetivo.GetComponent<dondeMover>().life / robotObjetivo.GetComponent<dondeMover>().totalLife;
                        robotObjetivo.transform.GetChild(0).transform.GetChild(1).transform.localScale =
                           new Vector3(actualLife, robotObjetivo.transform.GetChild(0).transform.GetChild(1).transform.localScale.y,
                           robotObjetivo.transform.GetChild(0).transform.GetChild(1).transform.localScale.z);
                        aux = true;
                    }
                }
            }

            aux = false;
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        throw new System.NotImplementedException();
    }

    public void sendRobotTurn(string data) {
        PhotonNetwork.RaiseEvent((byte)7, data, new Photon.Realtime.RaiseEventOptions { Receivers = Photon.Realtime.ReceiverGroup.Others }, new ExitGames.Client.Photon.SendOptions() { });
    }

    public void sendInformation(string data) {
        if (!PhotonNetwork.IsMasterClient) {
            PhotonNetwork.RaiseEvent((byte)5, data, new Photon.Realtime.RaiseEventOptions { Receivers = Photon.Realtime.ReceiverGroup.Others }, new ExitGames.Client.Photon.SendOptions() { });
        }
        else {
            PhotonNetwork.RaiseEvent((byte)6, data, new Photon.Realtime.RaiseEventOptions { Receivers = Photon.Realtime.ReceiverGroup.Others }, new ExitGames.Client.Photon.SendOptions() { });
        }
    }

    public void sendRobotLife(float data, GameObject enemy) {
        object[] inf = new object[2];
        inf[0] = data;
        inf[1] = enemy.GetComponent<dondeMover>().myId;
        PhotonNetwork.RaiseEvent((byte)8, inf, new Photon.Realtime.RaiseEventOptions { Receivers = Photon.Realtime.ReceiverGroup.Others }, new ExitGames.Client.Photon.SendOptions() { });
    }
}
