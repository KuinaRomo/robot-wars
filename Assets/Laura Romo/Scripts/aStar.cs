﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aStar : MonoBehaviour {

    List<List<int>> map;
    [SerializeField] GameObject tablero;
    Transform myTransform;
    //Game objects temporales que luego pasara el enemigo cuando llame la función
    
    private int steps;
    private int actualNode;
    private int mapX;
    private int mapY;
    private Node final;
    private List<Node> openList;
    private List<Node> closeList;
    private List<Node> pathList;
    private bool lastStep;
    private bool pullFirst;
    private bool isIn;
    private int distance;
    //private int node;
    private bool startRoad;
    private int nodeInClose;
    
    // Use this for initialization
    void Start () {
        //node = 0;
        actualNode = 0;
        pullFirst = false;
        isIn = false;
        //Leemos el tablero y creamos un mapa de colisiones
        map = new List<List<int>>();
        openList = new List<Node>();
        closeList = new List<Node>();
        myTransform = tablero.GetComponent<Transform>();
        for (int i = 0; i < myTransform.childCount; i++) {
            map.Add(new List<int>());
            for (int j = 0; j < myTransform.GetChild(i).childCount; j++) {
               
                if(myTransform.GetChild(i).GetChild(j).tag == "block" || myTransform.GetChild(i).GetChild(j).tag == "water") {
                    map[i].Add(1);
                    Debug.Log(myTransform.GetChild(i).GetChild(j).tag);
                }
                else {
                    map[i].Add(0);
                }
            }
            //Debug.Log(map[i]);
        }
        //Debug.Log(map[1][0]);
        mapX = map.Count;
        mapY = map[0].Count;
        //Llamando a la funcion prueba
    }
    
	// Update is called once per frame
	void Update () {
                
	}

    /*Función que calcula el aStar entre 2 personajes o un personaje y una torre y devuelve
     la distancia. Debera recibir por parametro un GameObject del objetivo.*/

    public int CalcularDistancia(GameObject finalRobot, GameObject enemyRobot, bool putInList, bool robot, bool robotEnemy, bool casilla, bool robotA) {
        pullFirst = false;
        if (finalRobot != null && enemyRobot != null) {
            openList = new List<Node>();
            closeList = new List<Node>();
            //Ponemos el final y el principio en el mapa de colisiones
            //Posicion inicial marcada con un 2
            int posXen = 0;
            int posYen = 0;
            if (robotEnemy) {
                posXen = enemyRobot.GetComponent<dondeMover>().numFila;
                posYen = enemyRobot.GetComponent<dondeMover>().numCol;
                map[posXen][posYen] = 2;
            }
            else {
                posXen = (int)enemyRobot.GetComponent<FactoryManager>().positions[0].transform.position.x;
                posYen = (int)enemyRobot.GetComponent<FactoryManager>().positions[0].transform.position.y;
                map[posXen][posYen] = 3;
            }
            //Posicion final se marca con un 3
            int posX = 0;
            int posY = 0;
            if (casilla) {
                posX = (int)finalRobot.transform.position.y;
                posY = (int)finalRobot.transform.position.x;
                map[posX][posY] = 3;
            }
            else if (robot) {
                posX = finalRobot.GetComponent<dondeMover>().numFila;
                posY = finalRobot.GetComponent<dondeMover>().numCol;
                map[posX][posY] = 3;
            }
            else {
                posX = (int)finalRobot.GetComponent<FactoryManager>().positions[0].transform.position.y;
                posY = (int)finalRobot.GetComponent<FactoryManager>().positions[0].transform.position.x;
                map[posX][posY] = 3;
            }

            //Creamos el node final.
            final = fillStruct(posX, posY, -1, 0, true);

            //Creamos el node inicial y lo metemos en la openList
            Node nodeIn = new Node();
            nodeIn = fillStruct(posXen, posYen, -1, 0, false);
            openList.Add(nodeIn);

            /*Empezamos el bucle del a*. Este ira recorriendo todas las casillas hasta que encuentre la 
             final y entonces saldra del bucle*/

            while (!lastStep) {
                if (!pullFirst) {
                    cheapToClose();
                    pullFirst = true;
                }
                else {
                    pullOrChange();
                    int nodes = 0;
                    for (int i = 0; i < openList.Count; i++) {
                        if (openList[i] != null) {
                            nodes++;
                        }
                    }
                    if (nodes == 0) {
                        lastStep = true;
                        break;
                    }
                    if (!lastStep) {
                        cheapToClose();
                    }
                }
                actualNode = closeList.Count - 1;
            }
            distance = closeList[closeList.Count - 1].getTotalCost();
            if (putInList) {
                startRoad = false;
                nodeInClose = closeList.Count - 1;
                if (robotA) {
                    enemyRobot.GetComponent<dondeMover>().pathRobot.Clear();
                }
                while (!startRoad) {
                    nodeInClose = closeList[nodeInClose].getFather();
                    if (nodeInClose != 0) {
                        if (robotEnemy && !robotA) {
                            enemyRobot.GetComponent<enemyPosition>().path.Add(nodeToGameObject(closeList[nodeInClose]));
                        }
                        else if(robotA) {
                            enemyRobot.GetComponent<dondeMover>().pathRobot.Add(nodeToGameObject(closeList[nodeInClose]));
                        }
                    }
                    else {
                        startRoad = true;
                    }
                }
            }
            lastStep = false;
            pullFirst = false;
            clearMap();
            return distance;
        }
        return -2;

    }

    void clearMap() {
        map.Clear();
        for (int i = 0; i < myTransform.childCount; i++) {
            map.Add(new List<int>());
            for (int j = 0; j < myTransform.GetChild(i).childCount; j++) {
                if (myTransform.GetChild(i).GetChild(j).tag == "block" || myTransform.GetChild(i).GetChild(j).tag == "water" || myTransform.GetChild(i).GetChild(j).tag == "ocupada") {
                    map[i].Add(1);
                }
                else {
                    map[i].Add(0);
                }
            }
        }
    }

    /*private int totalCoste() {
        bool startRoad = false;
        int firstStep = closeList.Count - 1;
        while (!startRoad) {
            firstStep = closeList[firstStep].getFather();
            if (firstStep != 0) {
                distance += closeList[firstStep].getTotalCost();
            }
            else
            {
                startRoad = true;
            }
        }
        return 0;
    }*/

    private bool checkCloseList(int x, int y) {
        isIn = false;
        for(int i = 0; i < closeList.Count; i++) {
            if(closeList[i].getY() == y && closeList[i].getX() == x) {
                isIn = true;
            }
        }
        return isIn;
    }

    void pullOrChange() {
        //int weight = -1;
        //int toClose = -1;
        bool final = false;
        
        if (closeList[closeList.Count - 1].getX() - 1 >= 0) {
            //Abajo
            bool close = checkCloseList(closeList[closeList.Count - 1].getX() - 1, closeList[closeList.Count - 1].getY());
            if (!close) {
                if (map[closeList[closeList.Count - 1].getX() - 1][closeList[closeList.Count - 1].getY()] == 3) {                    //Crear nodo que sera el final, meterlo en lista cerrada y poner final a true
                    Node node = new Node();
                    node = fillStruct(closeList[closeList.Count - 1].getX() - 1, closeList[closeList.Count - 1].getY(),
                        closeList.Count - 1, 10, false);
                    closeList.Add(node);
                    lastStep = true;
                    final = true;
                }

                else if (map[closeList[closeList.Count - 1].getX() - 1][closeList[closeList.Count - 1].getY()] != 1 && !final) {
                    //Abajo
                    if (!checkOpenList(closeList[closeList.Count - 1].getX() - 1, closeList[closeList.Count - 1].getY())) {
                        Node node = new Node();
                        node = fillStruct(closeList[closeList.Count - 1].getX() - 1, closeList[closeList.Count - 1].getY(),
                            closeList.Count - 1, 10, false);
                        openList.Add(node);
                    }
                    else {
                        /*Comprueba si esta g es superior a ir a la otra casilla. Si no lo es lo pongo en
                        la close list y le pongo de padre lo que tiviera de padre el padre de la actual*/
                        int auxG = closeList[closeList.Count - 1].getCost() + 10;
                        if (auxG < openList[actualNode].getCost()) {
                            //Se cambia todo
                            openList[actualNode].setFather(closeList.Count - 1);
                            openList[actualNode].setCost(auxG);
                            openList[actualNode].setTotalCost(openList[actualNode].getCost() + openList[actualNode].getTotalCost());
                        }
                    }
                }
            }

            /*if (map[closeList[closeList.Count - 1].getX() - 1][closeList[closeList.Count - 1].getY()] == 1) {
                up = true;
            }*/
        }

        if (closeList[closeList.Count - 1].getX() + 1 < mapX) {
            bool close = checkCloseList(closeList[closeList.Count - 1].getX() + 1, closeList[closeList.Count - 1].getY());
            if (!close) {
                if (map[closeList[closeList.Count - 1].getX() + 1][closeList[closeList.Count - 1].getY()] == 3) {
                    //Crear nodo que sera el final, meterlo en lista cerrada y poner final a true
                    Node node = new Node();
                    node = fillStruct(closeList[closeList.Count - 1].getX() + 1, closeList[closeList.Count - 1].getY(),
                        closeList.Count - 1, 10, false);
                    closeList.Add(node);
                    final = true;
                    lastStep = true;
                }
                else if (map[closeList[closeList.Count - 1].getX() + 1][closeList[closeList.Count - 1].getY()] != 1 && !final) {
                    //Arriba
                    if (!checkOpenList(closeList[closeList.Count - 1].getX() + 1, closeList[closeList.Count - 1].getY())) {
                        Node node = new Node();
                        node = fillStruct(closeList[closeList.Count - 1].getX() + 1, closeList[closeList.Count - 1].getY(),
                            closeList.Count - 1, 10, false);
                        openList.Add(node);
                    }
                    else {
                        /*Compruebas la g actual y te aseguras que no sea menor que la que ya tiene.
                        Si es menor se le cambia el padre por la actual casilla estudiada y se recalcula
                        su */
                        int auxG = closeList[closeList.Count - 1].getCost() + 10;
                        if (auxG < openList[actualNode].getCost()) {
                            //Se cambia todo
                            openList[actualNode].setFather(closeList.Count - 1);
                            openList[actualNode].setCost(auxG);
                            openList[actualNode].setTotalCost(openList[actualNode].getCost() + openList[actualNode].getTotalCost());
                        }
                    }
                }
            }

            /*if (map[closeList[closeList.Count - 1].getX() + 1][closeList[closeList.Count - 1].getY()] == 1) {
                down = true;
            }*/

        }

        if (closeList[closeList.Count - 1].getY() - 1 >= 0) {
            bool close = checkCloseList(closeList[closeList.Count - 1].getX(), closeList[closeList.Count - 1].getY() - 1);
            if (!close) {
                if (map[closeList[closeList.Count - 1].getX()][closeList[closeList.Count - 1].getY() - 1] == 3) {
                    //Crear nodo que sera el final, meterlo en lista cerrada y poner final a true
                    Node node = new Node();
                    node = fillStruct(closeList[closeList.Count - 1].getX(), closeList[closeList.Count - 1].getY() - 1,
                        closeList.Count - 1, 10, false);
                    closeList.Add(node);
                    final = true;
                    lastStep = true;
                }
                else if (map[closeList[closeList.Count - 1].getX()][closeList[closeList.Count - 1].getY() - 1] != 1 && !final) {
                    //Izquierda
                    if (!checkOpenList(closeList[closeList.Count - 1].getX(), closeList[closeList.Count - 1].getY() - 1)) {
                        Node node = new Node();
                        node = fillStruct(closeList[closeList.Count - 1].getX(), closeList[closeList.Count - 1].getY() - 1,
                            closeList.Count - 1, 10, false);
                        openList.Add(node);
                    }
                    else {
                        /*Comprueba si esta g es superior a ir a la otra casilla. Si no lo es lo pongo en
                        la close list y le pongo de padre lo que tiviera de padre el padre de la actual*/
                        int auxG = closeList[closeList.Count - 1].getCost() + 10;
                        if (auxG < openList[actualNode].getCost()) {
                            //Se cambia todo
                            openList[actualNode].setFather(closeList.Count - 1);
                            openList[actualNode].setCost(auxG);
                            openList[actualNode].setTotalCost(openList[actualNode].getCost() + openList[actualNode].getTotalCost());
                        }
                    }
                }
            }

            /*if (map[closeList[closeList.Count - 1].getX()][closeList[closeList.Count - 1].getY() - 1] == 1) {
                left = true;
            }*/
        }

        if (closeList[closeList.Count - 1].getY() + 1 < mapY) {
            bool close = checkCloseList(closeList[closeList.Count - 1].getX(), closeList[closeList.Count - 1].getY() + 1);
            if (!close) {
                if (map[closeList[closeList.Count - 1].getX()][closeList[closeList.Count - 1].getY() + 1] == 3) {
                    //Crear nodo que sera el final, meterlo en lista cerrada y poner final a true
                    Node node = new Node();
                    node = fillStruct(closeList[closeList.Count - 1].getX(), closeList[closeList.Count - 1].getY() + 1,
                        closeList.Count - 1, 10, false);
                    closeList.Add(node);
                    final = true;
                    lastStep = true;
                }

                else if (map[closeList[closeList.Count - 1].getX()][closeList[closeList.Count - 1].getY() + 1] != 1 && !final) {
                    //Derecha
                    if (!checkOpenList(closeList[closeList.Count - 1].getX(), closeList[closeList.Count - 1].getY() + 1)) {
                        Node node = new Node();
                        node = fillStruct(closeList[closeList.Count - 1].getX(), closeList[closeList.Count - 1].getY() + 1,
                            closeList.Count - 1, 10, false);
                        openList.Add(node);
                    }
                    else {
                        /*Comprueba si esta g es superior a ir a la otra casilla. Si no lo es lo pongo en
                        la close list y le pongo de padre lo que tiviera de padre el padre de la actual*/
                        int auxG = closeList[closeList.Count - 1].getCost() + 10;
                        if (auxG < openList[actualNode].getCost()) {
                            //Se cambia todo
                            openList[actualNode].setFather(closeList.Count - 1);
                            openList[actualNode].setCost(auxG);
                            openList[actualNode].setTotalCost(openList[actualNode].getCost() + openList[actualNode].getTotalCost());
                        }
                    }
                }
            }

            /*if (map[closeList[closeList.Count - 1].getX()][closeList[closeList.Count - 1].getY() + 1] == 1) {
                right = true;
            }*/
        }
    }

    /*private bool checkOpenList(int x, int y) {
        bool isIn = false;
        for (int i = 0; i < openList.Count; i++) {
            if (openList[i] != null) {
                if (openList[i].getX() == x && openList[i].getY() == y) {
                    isIn = true;
                    node = i;
                }
            }
        }
        return isIn;
    }*/

    bool checkOpenList(int x, int y) {
        bool isIn = false;
        for (int i = 0; i < openList.Count; i++) {
            if (openList[i] != null) {
                if (openList[i].getX() == x && openList[i].getY() == y) {
                    isIn = true;
                    actualNode = i;
                }
            }
        }
        return isIn;
    }

    private Node fillStruct(int x, int y, int father, int cost, bool end)  {
        Node node = new Node();
        node.setX(x);
        node.setY(y);

        if (!end) {
            if (father == -1) {
                node.setCost(cost);
            }
            else {
                node.setCost(closeList[father].getCost() + cost);
            }
            node.setManhattan(Mathf.Abs(x - final.getX()) + Mathf.Abs(y - final.getY()));
            node.setTotalCost(node.getCost() + node.getManhattan());
            node.setFather(father);
        }
        else {
            node.setCost(0);
        }
        return node;
    }

    void cheapToClose() {
        int weight = -1;
        int toClose = -1;
        for (int i = 0; i < openList.Count; i++) {
            if (openList[i] != null) {
                if (openList[i].getTotalCost() < weight || weight == -1) {
                    weight = openList[i].getTotalCost();
                    toClose = i;
                }
            }
        }
        
        if (toClose != -1) {
            closeList.Add(openList[toClose]);
            openList[toClose] = null;
        }
    }

    private GameObject nodeToGameObject(Node node) {
        //x = myTransform.GetChild(i) and y = myTransform.GetChild(i).GetChild(j);
        GameObject casilla;
        casilla = myTransform.GetChild(node.getX()).GetChild(node.getY()).gameObject;
        return casilla;
    }

}
