﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCamImg : MonoBehaviour {

    float xPosition;
    float yPosition;
    float xCamPos;
    float yCamPos;
    [SerializeField] GameObject camera;
    RectTransform transform;

    // Start is called before the first frame update
    void Start()  {
        transform = this.GetComponent<RectTransform>();
        xPosition = 0;
        yPosition = 0;
    }

    // Update is called once per frame
    void Update() {
        //transform.TransformVector =
        calculatePosition();
        this.GetComponent<RectTransform>().localPosition = new Vector3(xPosition, yPosition, 0);
    }

    void calculatePosition() {
        xPosition = (float)(camera.transform.position.x - 4.5) * 4;
        yPosition = (float)(camera.transform.position.y - 4.5) * 4;
    }
}
