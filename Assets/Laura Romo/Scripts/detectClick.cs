﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class detectClick : MonoBehaviour {
    private GameObject gameManager;
    [SerializeField] GameObject actionHud;
    [SerializeField] CameraMovement cameraMovement;
    [SerializeField] GameObject RobotInf;
    public bool pause;
    private bool HudAbierto;
    //private bool hudAppeared;

    // Use this for initialization
    void Start () {
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
        actionHud = GameObject.Find("hudActions");
        RobotInf = GameObject.Find("RobotInf");
        RobotInf.SetActive(false);
        actionHud.SetActive(false);
        pause = false;
        //hudAppeared = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0) && !pause) {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider != null) {
                makeThings(hit.collider.gameObject);
            }
        }
    }

    void showRobot(Robot actualRobot) {
        GameObject tmp;
        if (actualRobot.torsoType == TypeRobot.AERODYNAMIC) {
            tmp = RobotInf.transform.GetChild(1).GetChild(0).GetChild(0).gameObject;
            tmp.SetActive(true);
        }
        else if(actualRobot.torsoType != TypeRobot.AERODYNAMIC) {
            tmp = RobotInf.transform.GetChild(1).GetChild(1).GetChild((int)actualRobot.torsoType).gameObject;
            tmp.SetActive(true);
        }
        RobotInf.transform.GetChild(1).GetChild(0).GetChild((int)actualRobot.headType + 1).gameObject.SetActive(true);
        RobotInf.transform.GetChild(1).GetChild(2).GetChild((int)actualRobot.legsType).gameObject.SetActive(true);

        //Show Inf
        RobotInf.transform.GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = "Live:" +
            actualRobot.getRobot().GetComponent<dondeMover>().life + "/" + actualRobot.getRobot().GetComponent<dondeMover>().totalLife;

        RobotInf.transform.GetChild(2).GetChild(1).GetComponent<TextMeshProUGUI>().text = "Move:" +
            actualRobot.getRobot().GetComponent<dondeMover>().numCasillas;

        RobotInf.transform.GetChild(2).GetChild(2).GetComponent<TextMeshProUGUI>().text = "Attack:" +
            actualRobot.getRobot().GetComponent<dondeMover>().attackPower;

        RobotInf.transform.GetChild(2).GetChild(3).GetComponent<TextMeshProUGUI>().text = "Range:" +
            actualRobot.getRobot().GetComponent<dondeMover>().numAttack;
    }

    void hideRobotUI() {
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < RobotInf.transform.GetChild(1).GetChild(i).childCount; j++) {
                RobotInf.transform.GetChild(1).GetChild(i).GetChild(j).gameObject.SetActive(false);
            }
        }
    }

    void makeThings(GameObject clickedObject) {
        //Debug.Log("The tag of the object is: " + clickedObject.gameObject.tag);
        //Si se hace click en el robot y se llama a la funcion que selecciona robot
        if(clickedObject.gameObject.GetComponent<dondeMover>() != null) {
            //Debug.Log("Movido: " + clickedObject.gameObject.GetComponent<dondeMover>().movido);
            //Debug.Log("Atacado: " + clickedObject.gameObject.GetComponent<dondeMover>().atacado);
            //Debug.Log("Turno: " + gameManager.GetComponent<GameManager>().robotTurn);
        }
        //Debug.Log("Turn: " + gameManager.GetComponent<GameManager>().robotTurn);
        if (gameManager.GetComponent<GameManager>().robotTurn == clickedObject.gameObject.tag) {
            if (!clickedObject.gameObject.GetComponent<dondeMover>().movido || !clickedObject.gameObject.GetComponent<dondeMover>().atacado) {
                if (gameManager.GetComponent<GameManager>().selected || HudAbierto) {
                    HudAbierto = false;
                    actionHud.SetActive(false);
                    RobotInf.SetActive(false);
                    hideRobotUI();
                    cameraMovement.moveCamera = true;
                    cameraMovement.changeSprite();
                }
                else {
                    if(gameManager.GetComponent<GameManager>().online && 
                        gameManager.GetComponent<GameManager>().robotsControled == gameManager.GetComponent<GameManager>().robotTurn) {
                        actionHud.SetActive(true);
                        RobotInf.SetActive(true);
                        showRobot(gameManager.GetComponent<GameManager>().returnRobot(clickedObject));
                        cameraMovement.moveCamera = false;
                        cameraMovement.changeSprite();
                        gameManager.gameObject.GetComponent<GameManager>().robotSelect = clickedObject.gameObject;
                    }
                    else if (!gameManager.GetComponent<GameManager>().online) {
                        HudAbierto = true;
                        actionHud.SetActive(true);
                        RobotInf.SetActive(true);
                        showRobot(gameManager.GetComponent<GameManager>().returnRobot(clickedObject));
                        cameraMovement.moveCamera = false;
                        cameraMovement.changeSprite();
                        gameManager.gameObject.GetComponent<GameManager>().robotSelect = clickedObject.gameObject;
                    }
                }
                /*if (!this.gameObject.GetComponent<dondeMover>().movido) {
                    if (!gameManager.GetComponent<GameManager>().selectRobot(this.gameObject, false)) {
                        Debug.Log("Is not your turn");
                    }
                }

                else {
                    gameManager.GetComponent<GameManager>().selectRobot(this.gameObject, true);
                }*/
            }
        }

        else if (clickedObject.gameObject.tag != "robotA" && clickedObject.gameObject.tag != "robotB" 
            && clickedObject.gameObject.tag != "factoryA" && clickedObject.gameObject.tag != "factoryB") {
            Debug.Log(clickedObject.gameObject.tag);
             if (!gameManager.GetComponent<GameManager>().moving && clickedObject.tag == "move") {
                 gameManager.GetComponent<GameManager>().casillaObjetivo = clickedObject.gameObject;
                 gameManager.GetComponent<GameManager>().moving = true;
                 if(clickedObject.tag == "move") {
                     actionHud.SetActive(false);
                     RobotInf.SetActive(false);
                     hideRobotUI();
                     cameraMovement.moveCamera = true;
                     cameraMovement.changeSprite();
                 }
             }
        }

        else if(clickedObject.gameObject.tag == "robotA" || clickedObject.gameObject.tag == "robotB") {
            gameManager.GetComponent<GameManager>().atacar(clickedObject.gameObject,
                gameManager.GetComponent<GameManager>().returnRobot(gameManager.GetComponent<GameManager>().robotSelect).weapon);
            if (gameManager.GetComponent<GameManager>().robotTurn != clickedObject.gameObject.tag) {
                actionHud.SetActive(false);
                RobotInf.SetActive(false);
                cameraMovement.moveCamera = true;
                cameraMovement.changeSprite();
            }
        }

        else if(clickedObject.gameObject.tag == "factoryA" || clickedObject.gameObject.tag == "factoryB") {
            if(gameManager.GetComponent<GameManager>().robotTurn == "robotA" && clickedObject.gameObject.tag == "factoryB" ||
                gameManager.GetComponent<GameManager>().robotTurn == "robotB" && clickedObject.gameObject.tag == "factoryA") {
                if(!clickedObject.GetComponent<FactoryManager>().death)
                    gameManager.GetComponent<GameManager>().attackFactory(clickedObject.gameObject);
            }
        }

    }
   
}
