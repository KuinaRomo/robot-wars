﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

//enum RobotTypes { NORMAL, AERODYNAMIC, TANK, UFO };

public class Robot {
    int id;
    int life;
    int move;
    int defence;
    int row;
    int col;
    int resorceNeeded;
    public int numRobotCreated;
    bool attacked = false;
    bool moved = false;
    GameObject robot;
    RobotParts robotParts;
    public List<int> attacks = new List<int>();
    public List<GameObject> parts;
    List<int> numOfParts = new List<int>();
    public List<string> AnimationArm;
    public List<string> AnimationWeapon;
    public List<string> blockedObjects;
    string dataSend;
    public TypeRobot headType;
    public TypeRobot torsoType;
    public TypeRobot legsType;
    public RobotWeapons weapon;
    //Debilidades
    public int debilityKnife = 0;
    public int debilitySword = 0;
    public int debilityGun = 0;
    public int debilityShotgun = 0;
    public int debilityMachineGun = 0;
    public int debilitySniper = 0;
    public int debilityLaser = 0;

    public void addAttacks(int attackPosition) {
        attacks.Add(attackPosition);
    }

    public Robot() {

    }

    public Robot(int lifeRbt, int moveRbt, int defenceRbt, int resNeed) {
        life = lifeRbt;
        move = moveRbt;
        defence = defenceRbt;
        resorceNeeded = resNeed;
    }

    public Robot(int lifeRbt, int moveRbt, int defenceRbt, int resNeed, TypeRobot _headType, TypeRobot _torsoType, TypeRobot _legsType, RobotWeapons _weapon) {
        life = lifeRbt;
        move = moveRbt;
        defence = defenceRbt;
        resorceNeeded = resNeed;
        headType = _headType;
        torsoType = _torsoType;
        legsType = _legsType;
        weapon = _weapon;
    }

    public void initRobotParts(TypeRobot _headType, TypeRobot _torsoType, TypeRobot _legsType, RobotWeapons _weapon, int numRobotCreated) {
        robotParts = GameObject.FindGameObjectWithTag("GameManager").GetComponent<RobotParts>();
        parts = new List<GameObject>();
        AnimationArm = new List<string>();
        AnimationWeapon = new List<string>();
        parts.Add(robotParts.RobotPrefabs[(int)_torsoType][numRobotCreated][(int)RobotZone.TORSO]);
        parts.Add(robotParts.RobotPrefabs[(int)_headType][numRobotCreated][(int)RobotZone.HEAD]);
        //parts.Add(robotParts.RobotArms[(int)torsoType][(int)weapon]);
        parts.Add(robotParts.RobotPrefabs[(int)_legsType][numRobotCreated][(int)RobotZone.LEGS]);
        AnimationArm.Add(robotParts.animationArmsIdle[(int)_weapon]);
        AnimationArm.Add(robotParts.animationArmsAttack[(int)_weapon]);
        AnimationWeapon.Add(robotParts.animationWeaponsIdle[(int)_weapon]);
        AnimationWeapon.Add(robotParts.animationWeaponsAttack[(int)_weapon]);

        blockedObjects = robotParts.objectsBlocked[(int)_legsType];

        //Calcular debilidades
        if(_headType == TypeRobot.NORMAL) {
            debilityKnife += 5;
            debilitySword += 5;
        }
        else if(_headType == TypeRobot.AERODYNAMIC) {
            debilityKnife += 5;
            debilitySword += 5;
            debilitySniper += 5;
            debilityLaser += 5;
        }
        else if(_headType == TypeRobot.OVNI) {
            debilitySniper += 5;
            debilityLaser += 5;
        }
        else if (_headType == TypeRobot.TANK) {
            debilityGun += 5;
            debilityShotgun += 5;
            debilityMachineGun += 5;
        }

        if (_torsoType == TypeRobot.NORMAL) {
            debilityKnife += 5;
            debilitySword += 5;
        }
        else if (_torsoType == TypeRobot.AERODYNAMIC) {
            debilityKnife += 5;
            debilitySword += 5;
            debilitySniper += 5;
            debilityLaser += 5;
        }
        else if (_torsoType == TypeRobot.OVNI) {
            debilitySniper += 5;
            debilityLaser += 5;
        }
        else if (_torsoType == TypeRobot.TANK) {
            debilityGun += 5;
            debilityShotgun += 5;
            debilityMachineGun += 5;
        }

        if (_legsType == TypeRobot.NORMAL) {
            debilityKnife += 5;
            debilitySword += 5;
        }
        else if (_legsType == TypeRobot.AERODYNAMIC) {
            debilityKnife += 5;
            debilitySword += 5;
            debilitySniper += 5;
            debilityLaser += 5;
        }
        else if (_legsType == TypeRobot.OVNI) {
            debilitySniper += 5;
            debilityLaser += 5;
        }
        else if (_legsType == TypeRobot.TANK){
            debilityGun += 5;
            debilityShotgun += 5;
            debilityMachineGun += 5;
        }
    }

    public void idleAnim() {
        robot.transform.GetChild(3).GetComponent<AnimationController>().armsAnim = AnimationArm[0];
        robot.transform.GetChild(3).GetComponent<AnimationController>().weaponsAnim = AnimationWeapon[0];
    }

    public void attackAnim() {
        robot.transform.GetChild(3).GetComponent<AnimationController>().armsAnim = AnimationArm[1];
        robot.transform.GetChild(3).GetComponent<AnimationController>().weaponsAnim = AnimationWeapon[1];
    }

    public void walkAnim() {
        robot.transform.GetChild(3).GetComponent<AnimationController>().armsAnim = "Walk";
        robot.transform.GetChild(3).GetComponent<AnimationController>().weaponsAnim = "Walk_1";
        robot.transform.GetChild(3).GetComponent<AnimationController>().weaponsAnim = "Walk_1_gun";
    }

    public Robot(string data) {
        //Si al final hacemos online se ha de añadir el resource del robot
        /*! dataSend: 
           - life: 3 primeros caracteres
           - move: siguiente caracter
           - defence: siguientes 3 caracters
           - col: siguiente caracter
           - row: siguiente caracter
           - añado tamaño array ataques
           - Ataques
           */
        Debug.Log("_________________________________________________________________________________");
        Debug.Log("Data: " + data);
        life = System.Convert.ToInt32(data.Substring(0, 3));
        Debug.Log("La vida és: " + life);
        move = System.Convert.ToInt32(data.Substring(3, 1));
        Debug.Log("Move: " + move);
        defence = System.Convert.ToInt32(data.Substring(4, 3));
        Debug.Log("Defence: " + defence);
        col = System.Convert.ToInt32(data.Substring(7, 1));
        Debug.Log("Col: " + col);
        row = System.Convert.ToInt32(data.Substring(8, 1));
        Debug.Log("Row: " + row);
        int aux = System.Convert.ToInt32(data.Substring(9, 1));
        for(int i = 1; i < aux + 1; i++) {
            attacks.Add(System.Convert.ToInt32(data.Substring((9 + i), 1)));
            Debug.Log("Attack: " + attacks[i - 1]);
        }
    }

    public void createNewRobot(int _move, int _defence) {
        move = _move;
        defence = _defence;
    }

    // getters y setters

    public int getLife() {
        return life;
    }

    public TypeRobot getHeadType() {
        return headType;
    }

    public void setLife(int _life) {
        life = _life;
    }

    public int getMove() {
        return move;
    }

    public void setMove(int _move) {
        move = _move;
    }

    public int getDefense() {
        return defence;
    }

    public void setDefense(int _defence) {
        defence = _defence;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int _row) {
        row = _row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int _col) {
        col = _col;
    }

    public bool getAttacked() {
        return attacked;
    }

    public void setAttacked(bool _attcked) {
        attacked = _attcked;
    }

    public bool getMoved() {
        return moved;
    }

    public void setMoved(bool _moved) {
        moved = _moved;
    }

    public GameObject getRobot() {
        return robot;
    }

    public void setRobot(GameObject _robot) {
        robot = _robot;
    }

    public void setId(int _id) {
        id = _id;
    }

    public int getId() {
        return id;
    }

    public int getResources() {
        return resorceNeeded;
    }

    public void setParts(List<GameObject> robotParts) {
        parts = robotParts;
    }

    public string getDataSend() {
        Debug.Log("Si vida es mayor que 999 o se move mayor que 10 o defence superior a 999" +
            "o hay mas de 99 ataques, menos de 10 ataques por robot peta online. " +
            "Avisad para arreglar. Es fàcil.");
        /*int life;
        int move;
        int defence;
        int row;
        int col;
        bool attacked = false;
        bool moved = false;*/
        /*! dataSend: 
            - life: 3 primeros caracteres
            - move: siguiente caracter
            - defence: siguientes 3 caracters
            - col: siguiente caracter
            - row: siguiente caracter
            - añado tamaño array ataques
            */
        dataSend = "";
        //! Añado life con 0 delante si es necesario
        if(life < 100 && life > 10) {
            dataSend += '0';
            dataSend += life;
        }
        else if(life < 10) {
            dataSend += "00";
            dataSend += life;
        }
        else {
            dataSend += life;
        }

        //! Añado move
        dataSend += move;

        //! Añado defence con 0 delante si es necesario
        if (defence < 100 && defence > 10) {
            dataSend += "0";
            dataSend += defence;
        }
        else if (defence < 10) {
            dataSend += "00";
            dataSend += defence;
        }
        else {
            dataSend += defence;
        }

        //! Añado col
        dataSend += col;

        //! Añado row
        dataSend += row;

        //! Añado tamaño array ataques
        dataSend += attacks.Count;

        //! Añado attacks
        for(int i = 0; i < attacks.Count; i++) {
            if(attacks[i] < 10) {
                dataSend += "0";
                dataSend += attacks[i];
            }
            else {
                dataSend += attacks[i];
            }
        }
        return dataSend;
    }

    public List<int> getAttacks() {
        return attacks;
    }
}
